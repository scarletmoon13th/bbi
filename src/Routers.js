import React, { Component } from "react"
import {
  PermissionsAndroid,
  CameraRoll,
  Text,
  AsyncStorage,
  TouchableOpacity,
  Dimensions,
  Platform
} from "react-native"
import { Scene, Router, Stack, Drawer, Actions } from "react-native-router-flux"
import Login from "./Temp/Login"
import Workslist from "./Temp/Workslist"
import WorkDetail from "./Temp/WorkDetail"
import Topiclist from "./Temp/Topiclist"
import Cameraroll from "./Temp/Cameraroll"
import Itemslist from "./Temp/Itemslist"

export default class Routers extends Component {
  componentDidMount() {
    this.requestCameraPermission()
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
        ],
        {
          title: " App Permission",
          message: "App needs access to phone feature "
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.warn("You can use the camera")
      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene key="login" component={Login} hideNavBar />
          <Scene
            key="worklist"
            component={Workslist}
            title="Works list"
            init={true}
            hideNavBar
            // renderLeftButton={() => {
            //   return <View />
            // }}
          />
          <Scene key="WorkDetail" component={WorkDetail} />
          <Scene key="topiclist" component={Topiclist} hideNavBar />
          <Scene key="itemslist" component={Itemslist} hideNavBar />
          <Scene
            key="Cameraroll"
            component={Cameraroll}
            title="Camera Roll"
            hideNavBar={true}
          />
        </Stack>
      </Router>
    )
  }
}
