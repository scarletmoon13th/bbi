import React, { Component } from "react"
import axios from "axios"
import { WORK, WORKERROR, MAIN_WORK, ERROR } from "./types"
import { API_IP } from "../Constants/Constant"
import { AsyncStorage } from "react-native"

export const fetchwork = token => {
  return async dispatch => {
    // console.(token)
    axios
      .get(API_IP + "Building?token=" + token)
      .then(response => {
        if (response.data.data != null) {
          // console.log("workList", response.data)
          return dispatch({
            type: WORK,
            payload: response.data.data
          })
        } else {
          return dispatch({
            type: WORKERROR,
            payload: response.message
          })
        }
      })
      .catch(err => {
        // console.log("login error", err)
        return dispatch({
          type: WORKERROR,
          payload: err.message
        })
      })
  }
}

export const fetchAlldata = token => {
  return async dispatch => {
    // console.(token)
    axios
      .get(API_IP + "Building/building_total?token=" + token)
      .then(async response => {
        // console.log(response.data)

        if (response.data.data != null) {
          // console.log("workList",response.data.data.buildings.length)
          let buildingadd = response.data.data
          if (response.data.data.buildings.length != 0) {
            await buildingadd.buildings.map(async (itembuildings, index) => {
              if (itembuildings.building_floors.length != 0) {
                await itembuildings.building_floors.map(async (itemfloors, index) => {
                  itemfloors.check = false
                
                  
                  if (itemfloors.building_checklists.length != 0) {
                    await itemfloors.building_checklists.map((item, index) => {
                      // item.check_server = false
                      item.building_id= itembuildings.building_id
                      console.log('add_check in',item)
                    })
                  }
                })
              }
            })
            let MAIN_WORK_DATA = JSON.stringify(buildingadd)
            await AsyncStorage.setItem("MAIN_DATA", MAIN_WORK_DATA)
            return dispatch({
              type: MAIN_WORK,
              payload: buildingadd
            })
          } else {
            let MAIN_WORK_DATA = JSON.stringify(response.data.data)
            await AsyncStorage.setItem("MAIN_DATA", MAIN_WORK_DATA)
            return dispatch({
              type: MAIN_WORK,
              payload: response.data.data
            })
          }
        } else {
          // console.log("work error", response.data)
          return dispatch({
            type: ERROR,
            payload: response.data.message
          })
        }
      })
      .catch(err => {
        // console.log("work error", err)
        return dispatch({
          type: ERROR,
          payload: err.message
        })
      })
  }
}
export const fetchAllplusold = data => {
  return async dispatch => {
    // console.(token)
    axios
      .get(API_IP + "Building/building_total?token=" + data.token)
      .then(async response => {
        if (response.data.data != null) {
          let newmaindata = response.data.data
          if (newmaindata.buildings.length != 0) {
            let alldata = await newmaindata.buildings.map(
              async (newbuilding, index) => {
                await data.olddata.map(async (olddata, index) => {
                  if (
                    olddata!=null&&olddata!=undefined&&
                    olddata.buildingsid == newbuilding.building_id &&
                    newbuilding.building_floors.length != 0
                  ) {
                    await newbuilding.building_floors.map(
                      async (newfloors, index) => {
                        if (
                          olddata.floorid == newfloors.building_floor_id &&
                          newfloors.building_checklists.length != 0
                        ) {
                          await newfloors.building_checklists.map(
                            async (newitem, index) => {
                              if (
                                olddata.itemsid == newitem.building_checklist_id
                              ) {
                                newitem.photos =
                                  olddata.photos != null &&
                                  olddata.photos != undefined
                                    ? olddata.photos
                                    : null
                                newitem.code =
                                  olddata.code != null &&
                                  olddata.code != undefined
                                    ? olddata.code
                                    : null
                                    // newitem
                                // console.warn('olddata',olddata)
                              }
                            }
                          )
                        }
                      }
                    )
                  }
                })
               
              }
            )

            await Promise.all(alldata).then(async result => {
              let alldatamap = await newmaindata.buildings.map(
                async (item, index) => {
                  if (item.building_floors.length != 0) {
                    await item.building_floors.map(async (item, index) => {
                      item.check = false
                      item.server = false

                      if (item.building_checklists.length != 0) {
                        await item.building_checklists.map((item, index) => {
                          item.check_server = false
                          // console.log('add_check in',item)
                        })
                      }
                    })
                  }
                }
              )
              await Promise.all(alldatamap).then(async result => {
                return dispatch({
                  type: MAIN_WORK,
                  payload: newmaindata
                })
              })
            })
          }

        
        } else {
         
          return dispatch({
            type: ERROR,
            payload: response.data.message
          })
        }
      })
      .catch(err => {
        console.log("login error", err)
        return dispatch({
          type: ERROR,
          payload: err.message
        })
      })
  }
}

export const saveMaindata = data => {
  return async dispatch => {
    // console.log("saveMaindata", data)
    return dispatch({
      type: MAIN_WORK,
      payload: data
    })
  }
}
