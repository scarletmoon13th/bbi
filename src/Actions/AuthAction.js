import React, { Component } from "react"
import axios from "axios"
import { LOGIN, ERROR_LOGIN, IMAGE, IMAGEERROR } from "./types"
import { API_IP } from "../Constants/Constant"
import { AsyncStorage } from "react-native"
export const loginfetch = data => {
  return async dispatch => {
    const logindata = new FormData()
    logindata.append("username", data.username)
    logindata.append("password", data.password)
    var CancelToken = axios.CancelToken 
    var cancel
    console.log("data_login",logindata,API_IP + "Authentication/login")
    axios
      .post(API_IP + "Authentication/login", logindata, {
        cancelToken: new CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          cancel = c
        })
      })
      .then(async response => {
        if (response.data.data != null) {
          let userdata = { username: data.username, password: data.password }
          let USER_DATA = JSON.stringify(userdata)
          await AsyncStorage.setItem("USER_DATA", USER_DATA)
          console.log("login",response.data)
          return dispatch({
            type: LOGIN,
            payload: response.data.data.token
          })
        } else {
          console.log("login_errors",response)
          return dispatch({
            type: ERROR_LOGIN,
            payload: response.data.message
          })
        }
      })
      .catch(err => {
        console.log("catch login error", err)
        if(err.message!=undefined){
          return dispatch({
            type: ERROR_LOGIN,
            payload: err.message
          })
        }else{
          return dispatch({
            type: ERROR_LOGIN,
            payload: "Network Error"
          })
        }
       
      })
      // setTimeout(() => {
      //   cancel()
      // }, 1000 *10)
  }
}
