//login
export const LOGIN = 'login'
export const ERROR_LOGIN = 'error_login'

//work list
export const WORK = 'work'
export const WORKERROR = 'workerror'

// topic list
export const TOPIC = 'topic'
export const TOPICERROR = 'topicerror'
export const IMAGE = 'image'
export const IMAGEERROR = 'imageerror'
export const COUNT = 'count'

// image check
export const CHECK = 'check'
export const CHECK_ERROR = 'checkerror'

//maindata
export const MAIN_WORK = 'main_work'
export const ERROR = 'error'

//savephto

export const PHOTO_SAVE = 'photo_save'
export const SAVE_FAIL = 'save_fail'

