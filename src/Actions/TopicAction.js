import React, { Component } from "react"
import axios from "axios"
import {
  TOPIC,
  TOPICERROR,
  IMAGE,
  IMAGEERROR,
  CHECK,
  CHECK_ERROR,
  COUNT
} from "./types"
import { API_IP } from "../Constants/Constant"
import moment from "moment"
import { AsyncStorage, Image } from "react-native"
import ImageResizer from "react-native-image-resizer"

export const fetchtopic = data => {
  return async dispatch => {
    axios
      .get(
        API_IP +
          "Building_checklist/index/" +
          data.buildingsID +
          "?token=" +
          data.token
      )
      .then(response => {
        if (response.data.data != null) {
          // console.log("response.data", response.data)
          return dispatch({
            type: TOPIC,
            payload: response.data.data
          })
        } else {
          return dispatch({
            type: TOPICERROR,
            payload: response.message
          })
        }
      })
      .catch(err => {
        // console.log("login error", err)
        return dispatch({
          type: TOPICERROR,
          payload: err.message
        })
      })
  }
}

export const fetchimage = data => {
  return async dispatch => {
    axios
      .get(
        API_IP +
          "Building_checklist_result/index/" +
          data.buildingsID +
          "?token=" +
          data.token
      )
      .then(response => {
        if (response.data.data != null) {
          return dispatch({
            type: IMAGE,
            payload: response.data.data
          })
        } else {
          return dispatch({
            type: IMAGEERROR,
            payload: response.message
          })
        }
      })
      .catch(err => {
        // console.log("login error", err)
        return dispatch({
          type: IMAGEERROR,
          payload: err.message
        })
      })
  }
}

export const saveallimage = data => {
  return async dispatch => {
    let code = null
    let newdate = new Date()
    let date = moment(newdate).format("YYYY-MM-DD HH:mm:ss")
    // console.log("saveallimage", data)

    const dataimage = new FormData()
    dataimage.append("building_checklist_id", data.building_checklist_id)
    // dataimage.append("status", "ACTIVATE")
    dataimage.append("capture_date", date)
    if (data.location != null) {
      dataimage.append("latitude", data.location.position.lat)
      dataimage.append("longitude", data.location.position.log)
    }

    // console.log("length",data.image.length)
    if (data.image != null) {
      let x = 0
      for (i = 0; i < data.image.length; i++) {
        // console.log("image[i]", data.image[i])
        if (
          data.image[i].image.image != undefined &&
          data.image[i].image.building_checklist_result_id == undefined
        ) {
          // console.log(
          //   x,
          //   data.image[i].image,
          //   data.image[i].image.building_checklist_result_id
          // )
          // let imagereq= require(data.image[i].image.image)
          // let timestamp_old = data.image[i].image.timestamp
          let timestamp_new = moment(
            data.image[i].image.timestamp,
            "DD-MM-YYYY HH:mm:ss"
          ).format("YYYYMMDDHHmmss")
          console.warn("timestamp_new", timestamp_new)
          var photo = {
            uri: data.image[i].image.image,
            type: "image/jpeg",
            name:
              Math.floor(Math.random() * 999 + 100).toString() +
              "_" +
              timestamp_new +
              ".jpg",
            capture_date: data.image[i].image.timestamp
          }
          console.warn("photo")
          if (
            data.image[i].image.code != null ||
            data.image[i].image.code != undefined
          ) {
          } else {
            dataimage.append("image_data[" + x + "]", photo)
          }

          ++x
        }
      }
    }
    // let y = 0
    // for(i=0;i<data.deleteimage.length;i++){

    //   dataimage.append("delete_images["+y+"]", data.deleteimage[i].id)
    //   ++y
    // }
    console.warn("dataimage", data.token)
    await axios
      .post(
        API_IP + "Building_checklist_result/commit/?token=" + data.token,
        dataimage
      )
      .then(response => {
        console.warn("commit", response.data)
        //     if (response.data.data != null) {
        if (response.data.code == "1x0000") {
          // return {check:response.data.code}
          code = response.data.code

          // return code
        } else {
          // return dispatch({
          //   type: CHECK_ERROR,
          //   payload: response.data.message
          // })
        }

        //       return dispatch({
        //         type: IMAGE,
        //         payload: response.data.data
        //       })
        //     } else {
        //       return dispatch({
        //         type: IMAGEERROR,
        //         payload: response.message
        //       })
        //     }
      })
      .catch(err => {
        // console.log("login error", err)
        return dispatch({
          type: CHECK_ERROR,
          payload: err.message
        })
      })

    return code
  }
}

export const sendimage = data => {
  return async dispatch => {
    data.checklists_item.photos.image.map(async (image_item, image_index) => {
      if (image_item.image != "etc" && image_item.image.code != "0x0000") {
        let newdate = new Date()
        let date = moment(newdate).format("YYYY-MM-DD HH:mm:ss")
        const dataimage = new FormData()
        dataimage.append("building_checklist_id", data.building_checklist_id)
        // dataimage.append("status", "ACTIVATE")
        
        if (data.checklists_item.photos.location != null) {
          dataimage.append(
            "latitude",
            data.checklists_item.photos.location.position.lat
          )
          dataimage.append(
            "longitude",
            data.checklists_item.photos.location.position.log
          )
        }

        let timestamp_new = moment(
          image_item.image.timestamp,
          "DD-MM-YYYY HH:mm:ss"
        ).format("YYYYMMDDHHmmss")
        let timestamp_new_info = moment(
          image_item.image.timestamp,
          "DD-MM-YYYY HH:mm:ss"
        ).format("YYYY-MM-DD HH:mm:ss")
        dataimage.append("capture_date", timestamp_new_info)
        // var photo = null
        // await Image.getSize(image_item.image.image, async (width, height) => {
        // console.log('width,height image',width, height)
        let image_info_resize = {
          image_item: image_item,
          timestamp_new: timestamp_new
        }
        let resize = await dispatch(resizerImage(image_info_resize))
        console.warn(resize)
        var photo = {
          uri: "file://" + resize,
          type: "image/jpeg",
          name:
            Math.floor(Math.random() * 999 + 100).toString() +
            "_" +
            timestamp_new +
            ".jpg"
        }
        dataimage.append("image_data", photo)
        let value = {
          data: data,
          image_item: image_item,
          image_index: image_index,
          dataimage: dataimage
        }

        let response = await dispatch(posttoAPI(value))
        console.warn("response_retrun", response)
        if (response.message == null) {
          dispatch(savelocal(response)).then(rep => {
            dispatch(countimage(response))
          })
        } else {
          return dispatch({
            type: CHECK_ERROR,
            payload: response.message,
            isLast:response.islast
          })
        }
        // await ImageResizer.createResizedImage(
        //   image_item.image.path,
        //   3456,
        //   4608,
        //   "JPEG",
        //   80,
        //   0,
        //   "/storage/emulated/0/BBI/Images/resize"
        // ).then(async response => {
        //   var photo = {
        //     uri: "file://" + response.path,
        //     type: "image/jpeg",
        //     name:
        //       Math.floor(Math.random() * 999 + 100).toString() +
        //       "_" +
        //       timestamp_new +
        //       ".jpg"
        //   }

        //   dataimage.append("image_data", photo)
        //   // console.warn("photo_data", photo)

        //   let value = {
        //     data: data,
        //     image_item: image_item,
        //     image_index: image_index,
        //     dataimage: dataimage
        //   }

        //   await dispatch(posttoAPI(value)).then(response => {
        //     console.warn("response_data_info", response)
        //   })
        // })
        // })
        // console.warn("photo_data_image", dataimage)
        // var photo = {
        //   uri: image_item.image.image,
        //   type: "image/jpeg",
        //   name: Math.floor(Math.random() * 999 + 100).toString()+"_"+timestamp_new + ".jpg",

        // }
        // dataimage.append("image_data", photo)

        // await
        //   axios
        //     .post(
        //       API_IP + "Building_checklist_result/set?token=" + data.token,
        //       dataimage
        //     ).then(async(response) => {
        //       console.warn(response)
        //       if(response.data.code=='0x0000'){

        //         let countdata = {
        //           building_id:data.building_id ,
        //           building_floor_id:data.building_floor_id,
        //           building_checklist_id:data.building_checklist_id,
        //           imageinfo:image_item,
        //           code:response.data.code

        //         }
        //         dispatch(savelocal(countdata))
        //         // .then(rep=>{

        //         // })
        //         dispatch(countimage(countdata))
        //       }else{

        //         console.log("return error", response.data.message)
        //         dispatch({
        //           type: CHECK_ERROR,
        //           payload: response.data.message
        //         })
        //       }

        //     }).catch(err => {
        //       console.log("set error",err.message)
        //       return dispatch({
        //         type: CHECK_ERROR,
        //         payload: err.message
        //       })
        //     })
      }
    })
  }
}

export const resizerImage = data => {
  return async dispatch => {
    let imageresizer = await ImageResizer.createResizedImage(
      data.image_item.image.path,
      3456,
      4608,
      "JPEG",
      80,
      0,
      "/storage/emulated/0/BBI/Images/resize"
    ).then(async response => {
      let newpath = response.path
      return newpath
    })
    return imageresizer
  }
}
export const posttoAPI = data => {
  return async dispatch => {
    // console.warn("photo_data", data)
    // let test = {test:'suck'}
    // return test
    let response = await axios
      .post(
        API_IP + "Building_checklist_result/set?token=" + data.data.token,
        data.dataimage
      )
      .catch(err => {
        let response = err.message
        return response
      })
    console.warn("posttoAPI_response", response.data)
    if (response.data != null) {
      if (response.data.code == "0x0000") {
        if (
          data.data.checklists_item.photos.image.length ==
          data.image_index + 1
        ) {
          let countdata = {
            building_id: data.data.building_id,
            building_floor_id: data.data.building_floor_id,
            building_checklist_id: data.data.building_checklist_id,
            imageinfo: data.image_item,
            code: response.data.code,
            islast: true
          }
          return countdata
        } else {
          let countdata = {
            building_id: data.data.building_id,
            building_floor_id: data.data.building_floor_id,
            building_checklist_id: data.data.building_checklist_id,
            imageinfo: data.image_item,
            code: response.data.code,
            islast: false
          }
          return countdata
        }
      } else {
        if (
          data.data.checklists_item.photos.image.length ==
          data.image_index + 1
        ) {
          let error = { message: response.data.message, islast: true }
          return error
        } else {
          let error = { message: response.data.message, islast: false }
          return error
        }
      }
    } else {
      if (
        data.data.checklists_item.photos.image.length ==
        data.image_index + 1
      ) {
        let error = { message: response, islast: true }
        return error
      } else {
        let error = { message: response, islast: false }
        return error
      }
    }

    // .then(async response => {
    //   // let code = response.data.code
    //   // console.warn(code)
    //   // return code
    //   if (response.data.code == "0x0000") {
    //     if (
    //       data.data.checklists_item.photos.image.length ==
    //       data.image_index + 1
    //     ) {
    //       let countdata = {
    //         building_id: data.data.building_id,
    //         building_floor_id: data.data.building_floor_id,
    //         building_checklist_id: data.data.building_checklist_id,
    //         imageinfo: data.image_item,
    //         code: response.data.code,
    //         islast: true
    //       }
    //       return countdata
    //     } else {
    //       let countdata = {
    //         building_id: data.data.building_id,
    //         building_floor_id: data.data.building_floor_id,
    //         building_checklist_id: data.data.building_checklist_id,
    //         imageinfo: data.image_item,
    //         code: response.data.code,
    //         islast: false
    //       }
    //       return countdata
    //     }
    //   } else {
    //     if (
    //       data.data.checklists_item.photos.image.length ==
    //       data.image_index + 1
    //     ) {
    //       let error = { message: response.data.message, islast: true }
    //       return error
    //     } else {
    //       let error = { message: response.data.message, islast: false }
    //       return error
    //     }
    //   }

    // return response.data
    //     console.log(
    //       "count_item",
    //       image_index + " / " + data.checklists_item.photos.image.length
    //     )
    //     // console.warn(response)
    //     if (response.data.code == "0x0000") {
    //       if (data.checklists_item.photos.image.length == image_index + 1) {
    //         let countdata = {
    //           building_id: data.building_id,
    //           building_floor_id: data.building_floor_id,
    //           building_checklist_id: data.building_checklist_id,
    //           imageinfo: image_item,
    //           code: response.data.code,
    //           islast: true
    //         }
    //         dispatch(savelocal(countdata)).then(data => {
    //           console.warn("countdata", data)
    //         })

    //         dispatch(countimage(countdata))
    //       } else {
    //         let countdata = {
    //           building_id: data.building_id,
    //           building_floor_id: data.building_floor_id,
    //           building_checklist_id: data.building_checklist_id,
    //           imageinfo: image_item,
    //           code: response.data.code
    //         }
    //         dispatch(savelocal(countdata)).then(data => {
    //           console.warn("countdata", data)
    //         })

    //         dispatch(countimage(countdata))
    //       }
    //     } else {
    //       console.log("return error", response.data.message)
    //       if (data.checklists_item.photos.image.length == image_index + 1) {
    //         dispatch({
    //           type: CHECK_ERROR,
    //           payload: response.data.message,
    //           islast: true
    //         })
    //       } else {
    //         dispatch({
    //           type: CHECK_ERROR,
    //           payload: response.data.message
    //         })
    //       }
    //     }
    // })
    // .catch(err => {
    //   if (
    //     data.data.checklists_item.photos.image.length ==
    //     data.image_index + 1
    //   ) {
    //     let error = { message: err.message, islast: true }
    //     return error
    //   } else {
    //     let error = { message: err.message, islast: false }
    //     return error
    //   }
    // console.log("set error", err.message)
    // if (data.checklists_item.photos.image.length == image_index + 1) {
    //   return dispatch({
    //     type: CHECK_ERROR,
    //     payload: err.message,
    //     islast: true
    //   })
    // } else {
    //   return dispatch({
    //     type: CHECK_ERROR,
    //     payload: err.message
    //   })
    // }
    // })
  }
}

export const savelocal = data => {
  return async dispatch => {
    let value = await AsyncStorage.getItem("PIC_DATA")
    let olddata = JSON.parse(value)

    await olddata.map(async (picdata, picdataindex) => {
      console.warn("picdata.buildingsid ", picdata)
      if (
        picdata != null &&
        picdata.buildingsid != null &&
        picdata.buildingsid == data.building_id &&
        picdata.floorid == data.building_floor_id &&
        picdata.itemsid == data.building_checklist_id
      ) {
        let picdata_list = await picdata.photos.image.map(image_item => {
          // if( image_item.image.pic_id == data.imageinfo.image.pic_id){
          image_item.image.code = data.code
          console.log("pic_item_local", picdata)
          // }
        })
        Promise.all(picdata_list).then(async res => {
          let PIC_DATA = JSON.stringify(olddata)
          console.log("olddata", olddata)
          await AsyncStorage.setItem("PIC_DATA", PIC_DATA)
        })
      }
    })
    return "finish"
    // Promise.all(olddata)
  }
}

export const countimage = data => {
  return async dispatch => {
    console.warn(data)
    return dispatch({
      type: COUNT,
      payload: data,
      isLast: data.islast
    })
  }
}

export const testtext = data => {
  return async dispatch => {
    let text = "text"
    await axios
      .get(API_IP + "Building/building_total?token=" + data)
      .then(async response => {
        text = "treu"
      })
    return text
  }
}
