import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  Button,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator,
  Dimensions,
  FlatList
} from "react-native"

import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import styles from "./styles/topic.style"
import { Icon } from "react-native-elements"
import { MAIN_COLOR, BLCK_COLOR, WHITE_COLOR } from "../Constants/Color"
import LinearGradient from "react-native-linear-gradient"
import KeepAwake from 'react-native-keep-awake'

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width
class Topiclist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      topic: [
        { name: "A" },
        { name: "B" },
        { name: "C" },
        { name: "D" },
        { name: "E" },
        { name: "F" },
        { name: "G" },
        { name: "H" }
      ],
      loading: true,
      onPress: false,
      title: null,
      first: true,
      itemids: null,
      fetchtopic: false,
      workname: null,
      checkall: false,
      random: null,
      contall: null,
      count: 0,
      Main_data: null,
      try_count: 0
    }
  }

  onItemPress = item => {
    // this.setState(
    //   {
    //     onPress: true,
    //     title: item.name,
    //     itemids: item.building_checklist_id
    //   },
    //   () => {
    //     let data = {
    //       token: this.props.token,
    //       buildingsID: item.building_checklist_id
    //     }
    //     this.props.fetchimage(data)
    //   }
    // )
    Actions.itemslist({
      buildingsid: this.props.buildingsid,
      floorid: item.building_floor_id,
      title: item.name,
      floors_name: this.props.title
    })
    // Actions.Cameraroll({ title: item.item.name })
  }

  async componentDidMount() {
    let value = await AsyncStorage.getItem("PIC_DATA")
    let JSONvalue = await JSON.parse(value)
    console.log("JSONvalue1", JSONvalue)
    // if (this.props.token != null && this.props.buildingsid != null) {
    //   let data = {
    //     token: this.props.token,
    //     buildingsID: this.props.buildingsid
    //   }
    //   this.setState(
    //     {
    //       fetchtopic: true
    //     },
    //     () => {
    //       this.props.fetchtopic(data)
    //     }
    //   )
    // }
    // if (nextProps.work != null) {
    //   if (nextProps.work.buildings.lenght != 0) {

    //   }
    // }
    if (this.props.title != null) {
      this.setState({ workname: this.props.title })
    }
    if (this.props.Mainwork != null) {
      this.setState({ Main_data: this.props.Mainwork })
    }

    if (
      this.props.buildingsid != null &&
      this.props.Mainwork.buildings != null
    ) {
      // this.props.Mainwork.buildings.map((item, index) => {
      //   if (item.building_id == this.props.buildingsid) {
      //     this.setState({ topic: item.building_floors, loading: false })
      //   }
      // })
      this.props.Mainwork.buildings.map(async (building_item, index) => {
        if (building_item.building_id == this.props.buildingsid) {
          let floorscheck = await building_item.building_floors.map(
            async (floors_item, index) => {
              let checkserver = false
              if (floors_item.building_checklists.length != 0) {
                let pic_check = []
                let length_check = []
                let checklist = await floors_item.building_checklists.map(
                  async (checklists_item, index) => {
                    console.log(
                      "checklists_item.photos.image",
                      checklists_item.photos
                    )
                    // checkserver = false
                    length_check.push(
                      checklists_item.photos != null &&
                        checklists_item.photos != undefined &&
                        checklists_item.photos.image != null &&
                        checklists_item.photos.image != undefined
                        ? checklists_item.photos.image.length
                        : 0
                    )
                    if (
                      checklists_item.photos != null &&
                      checklists_item.photos != undefined &&
                      checklists_item.photos.image != null &&
                      checklists_item.photos.image != undefined &&
                      checklists_item.photos.image.length != 0
                    ) {
                      await checklists_item.photos.image.map(
                        (image_item, image_index) => {
                          if (image_item.image.code == "0x0000") {
                            // checkserver = true
                            console.log("image_item", true, image_item.image)
                            pic_check.push(true)
                          } else if (image_item.image != "etc") {
                            console.log("image_item", false, image_item.image)
                            // checkserver = false
                            pic_check.push(false)
                          }
                        }
                      )
                    }
                  }
                )

                Promise.all(checklist).then(result => {
                  // floors_item.server = checkserver
                  // console.log(floors_item.name, pic_check, length_check)
                  // console.warn(length_check.every(this.lengthcheck0value),length_check.some(this.lengthcheckvalue), pic_check.some(this.checkvalue))
                  // console.warn(
                  //   length_check.every(this.lengthcheck0value),
                  //   length_check.some(this.lengthcheckvalue)
                  // )
                  if (
                    length_check.every(this.lengthcheck0value) == true &&
                    length_check.some(this.lengthcheckvalue) == false
                  ) {
                    floors_item.server = "0"
                  } else if (
                    length_check.some(this.lengthcheckvalue) == true &&
                    pic_check.some(this.checkvalue) == true &&
                    pic_check.length != 0
                  ) {
                    floors_item.server = "2"
                  } else {
                    floors_item.server = "1"
                  }
                })
              }
            }
          )
          Promise.all(floorscheck).then(result => {
            this.setState({
              topic: building_item.building_floors,
              loading: false
            })
          })
          // this.setState({ topic: item.building_floors, loading: false })
        }
      })
    }
  }

  checkvalue = currentValue => {
    return currentValue == true
  }
  lengthcheck0value = currentValue => {
    return currentValue == 0
  }
  lengthcheckvalue = currentValue => {
    return currentValue > 0
  }

  async componentWillReceiveProps(nextProps) {
    // console.warn("nextProps")

    if (nextProps.test != null) {
      console.warn("nextProps", nextProps.test)
    }
    if (nextProps.Mainwork != null) {
      nextProps.Mainwork.buildings.map(async (building_item, index) => {
        if (building_item.building_id == this.props.buildingsid) {
          let floorscheck = await building_item.building_floors.map(
            async (floors_item, index) => {
              let checkserver = false
              if (floors_item.building_checklists.length != 0) {
                let pic_check = []
                let length_check = []
                let checklist = await floors_item.building_checklists.map(
                  async (checklists_item, index) => {
                    // console.log("checklists_item.photos.image", checklists_item.photos.image)
                    // checkserver = false
                    length_check.push(
                      checklists_item.photos != null &&
                        checklists_item.photos != undefined &&
                        checklists_item.photos.image != null &&
                        checklists_item.photos.image != undefined
                        ? checklists_item.photos.image.length
                        : 0
                    )
                    if (
                      checklists_item.photos != null &&
                      checklists_item.photos != undefined &&
                      checklists_item.photos.image != null &&
                      checklists_item.photos.image != undefined &&
                      checklists_item.photos.image.length != 0
                    ) {
                      await checklists_item.photos.image.map(
                        (image_item, image_index) => {
                          if (image_item.image.code == "0x0000") {
                            // checkserver = true
                            console.log("image_item", true, image_item.image)
                            pic_check.push(true)
                          } else if (image_item.image != "etc") {
                            console.log("image_item", false, image_item.image)
                            // checkserver = false
                            pic_check.push(false)
                          }
                        }
                      )
                    }
                  }
                )

                Promise.all(checklist).then(result => {
                  // floors_item.server = checkserver
                  // console.log(floors_item.name, pic_check, length_check)
                  // console.warn(length_check.every(this.lengthcheck0value),length_check.some(this.lengthcheckvalue), pic_check.some(this.checkvalue))
                  // console.warn(
                  //   length_check.every(this.lengthcheck0value),
                  //   length_check.some(this.lengthcheckvalue)
                  // )
                  if (
                    length_check.every(this.lengthcheck0value) == true &&
                    length_check.some(this.lengthcheckvalue) == false
                  ) {
                    floors_item.server = "0"
                  } else if (
                    length_check.some(this.lengthcheckvalue) == true &&
                    pic_check.some(this.checkvalue) == true &&
                    pic_check.length != 0
                  ) {
                    floors_item.server = "2"
                  } else {
                    floors_item.server = "1"
                  }
                })
              }
            }
          )
          Promise.all(floorscheck).then(result => {
            this.setState({
              topic: building_item.building_floors
            })
          })
        }
      })
    }

    if (nextProps.photocount != null && this.state.loading == true) {
      let Main_data = this.state.Main_data
      await Main_data.buildings.map(async (buildings_item, buildings_index) => {
        // console.warn(buildings_item.building_id==nextProps.photocount.building_id)
        if (buildings_item.building_id == nextProps.photocount.building_id) {
          await buildings_item.building_floors.map(
            async (floors_items, floors_index) => {
              if (
                floors_items.building_floor_id ==
                nextProps.photocount.building_floor_id
              ) {
                await floors_items.building_checklists.map(
                  async (checklists_item, index) => {
                    if (
                      checklists_item.building_checklist_id ==
                      nextProps.photocount.building_checklist_id
                    ) {
                      await checklists_item.photos.image.map(
                        async image_item => {
                          // console.log('checklists_item',checklists_item.photos.image)
                          if (
                            image_item.image.pic_id ==
                              nextProps.photocount.imageinfo.image.pic_id &&
                            image_item.image.code != "0x0000"
                          ) {
                            image_item.image.code = nextProps.photocount.code
                            // await this.props.saveMaindata(
                            //   Main_data
                            // )

                            this.setState(
                              {
                                Main_data: Main_data,
                                count: this.state.count + 1
                              },
                              async () => {
                                KeepAwake.deactivate()
                                if (this.state.count == this.state.contall) {
                                  await this.props.saveMaindata(
                                    this.state.Main_data
                                  )
                                  let MAIN_WORK_DATA = JSON.stringify(Main_data)
                                  // console.log('Main_data_savelocal',MAIN_WORK_DATA)
                                  await AsyncStorage.setItem(
                                    "MAIN_DATA",
                                    MAIN_WORK_DATA
                                  )
                                  await this.setState({
                                    loading: false,
                                    try_count: 0,
                                    count: 0 
                                  })
                                  // await this.state.Main_data.buildings.map(
                                  //   async (buildings_item, buildings_index) => {
                                  //     // console.warn(buildings_item.building_id==nextProps.photocount.building_id)
                                  //     if (
                                  //       buildings_item.building_id ==
                                  //       nextProps.photocount.building_id
                                  //     ) {
                                  //       await buildings_item.building_floors.map(
                                  //         async (
                                  //           floors_items,
                                  //           floors_index
                                  //         ) => {
                                  //           if (
                                  //             floors_items.building_floor_id ==
                                  //             nextProps.photocount
                                  //               .building_floor_id
                                  //           ) {
                                  //             await floors_items.building_checklists.map(
                                  //               async (
                                  //                 checklists_item,
                                  //                 index
                                  //               ) => {
                                  //                 console.log(
                                  //                   "Main_data_savelocal",
                                  //                   checklists_item.photos.image
                                  //                 )
                                  //               }
                                  //             )
                                  //           }
                                  //         }
                                  //       )
                                  //     }
                                  //   }
                                  // )
                                } else if (
                                  nextProps.isLast == true &&
                                  this.state.count != this.state.contall
                                ) {
                                  this.trySending()
                                }
                              }
                            )
                          }
                        }
                      )
                    }
                  }
                )
              }
            }
          )
        }
      })
    }

    if (nextProps.checkerror != null) {
      // Alert.alert(
      //   "Error",
      //   nextProps.checkerror,
      //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      //   { cancelable: false }
      // )
      if (nextProps.isLast == true && this.state.count != this.state.contall) {
        this.trySending()
      }
    }
  }

  render() {
    if (this.state.loading == true) {
      return (
        <View
          style={[
            styles.container,
            { justifyContent: "center", alignItems: "center" }
          ]}
        >
          <ActivityIndicator size="large" color={MAIN_COLOR} />
          <Text>
            {this.state.contall !== null && this.state.count}
            {this.state.contall != null && "/"}
            {this.state.contall != null && this.state.contall}
          </Text>
          {this.state.try_count > 0 && (
            <Text style={{ marginTop: 5 }}>
              {"มีข้อผิดพลาดในการส่งรูป " +
                "\n" +
                "กำลังส่งใหม่รอบที่ " +
                this.state.try_count}
            </Text>
          )}
        </View>
      )
    }
    // console.warn(this.state.workname)
    return (
      <View style={[styles.container, {}]}>
        <View style={[styles.navigation, styles.navigationBlue]}>
          <View style={styles.navigationTitle}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.buttonRight}
              onPress={() => {
                Actions.pop()
              }}
            >
              <Icon name="arrow-left" type="font-awesome" color="#fff" />
            </TouchableOpacity>

            <Text style={styles.navigationTitleText}>
              {this.state.workname}
            </Text>
          </View>
        </View>
        <View style={styles.selectallbox}>
          <TouchableOpacity
            style={[styles.checkbox]}
            onPress={() => {
              this.oncheckall()
            }}
          >
            {this.state.checkall == false ? (
              <Icon name="square" type="font-awesome" color={BLCK_COLOR} />
            ) : (
              <Icon
                name="check-square"
                type="font-awesome"
                color={BLCK_COLOR}
              />
            )}
          </TouchableOpacity>
          <Text>{"เลือกทั้งหมด"}</Text>
        </View>

        <FlatList
          data={this.state.topic}
          renderItem={this.renderItem}
          numColumns={2}
          style={styles.boxRow}
          keyExtractor={index => {
            index.toString()
          }}
          keyboardDismissMode="on-drag"
          extraData={this.state}
        />

        <TouchableOpacity
          style={styles.submitbut}
          onPress={() => {
            Alert.alert(
              "",
              "ยืนยันการส่งรูป",
              [
                { text: "ยกเลิก", onPress: () => console.log("no Pressed") },
                {
                  text: "ตกลง",
                  onPress: () => {
                    this.setState({ loading: true }, async () => {
                      console.warn(this.state.loading)
                      this.sendtoserver()
                    })
                   
                  }
                }
              ],
              { cancelable: false }
            )
          }}
        >
          <Text style={styles.submittext}>ส่ง</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderItem = ({ item, index }) => {
    // console.log("item.check", item.name, item.check)
    return (
      <View style={[styles.titlebox]}>
        <TouchableOpacity
          style={[styles.checkbox]}
          onPress={() => {
            this.oncheck(index)
          }}
        >
          {item.check == false ? (
            <Icon name="square" type="font-awesome" color={BLCK_COLOR} />
          ) : (
            <Icon name="check-square" type="font-awesome" color={BLCK_COLOR} />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.titleinnerbox}
          onPress={() => {
            this.onItemPress(item)
            // Actions.itemslist()
          }}
        >
          <View style={styles.Toptitlebox}>
            <Text style={styles.text}>{item.name}</Text>
          </View>
          <View style={styles.endtitlebox}>
            <Text style={[styles.text, { fontSize: (width / 360) * 12 }]}>
              {item.server == "2"
                ? "ส่งรูปแล้ว "
                : item.server == "1"
                ? "มีรูปยังไม่ได้ส่ง "
                : "ยังไม่มีรูป "}
            </Text>
            <View
              style={[
                styles.serv,
                item.server == "1" && { backgroundColor: "yellow" },
                item.server == "2" && { backgroundColor: "green" }
              ]}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  async oncheck(index) {
    // console.log(newArray[index])
    let newArray = this.state.topic
    let checkall = true
    newArray[index].check = !newArray[index].check
    // console.log(newArray[index])
    await newArray.map((item, index) => {
      if (item.check == false) {
        checkall = false
      }
    })

    this.setState({ topic: newArray, checkall: checkall }, () => {
      // console.log(newArray[index].check)
    })
  }

  async oncheckall() {
    // console.log(index)
    let newArray2 = this.state.topic
    if (this.state.checkall == true) {
      await newArray2.map((item, index) => {
        item.check = false
      })
      this.setState({ topic: newArray2, checkall: false }, () => {})
    } else {
      await newArray2.map((item, index) => {
        item.check = true
      })
      this.setState({ topic: newArray2, checkall: true }, () => {})
    }
  }

  promiselast = async topicmap => {
    return Promise.all(topicmap).then(result => {
      console.log("Promise")
      this.props.Mainwork.buildings.map(async (item, index) => {
        if (item.building_id == this.props.buildingsid) {
          let floorscheck = await item.building_floors.map(
            async (item, index) => {
              let checkserver = false
              if (item.building_checklists.length != 0) {
                let checklist = await item.building_checklists.map(
                  async (item, index) => {
                    if (
                      item.code != null &&
                      item.code != undefined &&
                      item.code == "1x0000"
                    ) {
                      checkserver = true
                    }
                  }
                )

                Promise.all(checklist).then(result => {
                  item.server = checkserver
                })
              }
            }
          )
          Promise.all(floorscheck).then(result => {
            this.setState({
              topic: item.building_floors,
              loading: false
            })
          })
        }
      })
      this.setState({
        loading: false
      })
    })
  }

  trySending = async () => {
    if (this.state.try_count <= 5) {
      this.setState({ try_count: this.state.try_count + 1, count: 0 }, () => {
        this.sendtoserver()
      })
    } else {
      KeepAwake.deactivate()
      await this.props.saveMaindata(this.state.Main_data)
      let MAIN_WORK_DATA = JSON.stringify(Main_data)
      await AsyncStorage.setItem("MAIN_DATA", MAIN_WORK_DATA)
      await this.setState({ loading: false, try_count: 0, count: 0 }, () => {
        Alert.alert(
          "มีข้อขัดข้อง",
          "กรุณาตรวจสอบสัญญาณอินเตอร์เน็ตก่อนส่งใหม่อีกครั้ง",
          [
            { text: "ยกเลิก", onPress: () => console.log("no Pressed") },
            {
              text: "ตกลง",
              onPress: () => {
                
                  this.sendtoserver()
                
              }
            }
          ],
          { cancelable: false }
        )
      })
    }
  }

  sendtoserver = async () => {
    let Main_data = this.props.Mainwork
    let value = await AsyncStorage.getItem("PIC_DATA")
    let JSONvalue = await JSON.parse(value)
    let newpicdata = JSONvalue
    // console.warn("JSONvalue", JSONvalue)
    KeepAwake.activate()
    await this.setState({ loading: true }, async () => {
      await this.coutallfun()
      if (this.state.contall == 0) {
        this.setState({ loading: false }, () => {
          return
        })
      }
      let topic_check = await this.state.topic.map(
        async (topic_check_item, topic_check_index) => {
          if (topic_check_item.check == true) {
            // console.warn(Main_data)
            let Main_data_map = await Main_data.buildings.map(
              async (buildings_item, buildings_index) => {
                if (buildings_item.building_id == this.props.buildingsid) {
                  // console.warn(buildings_item.building_id)
                  let floors_data_map = await buildings_item.building_floors.map(
                    async (floors_items, floors_index) => {
                      if (
                        floors_items.building_floor_id ==
                        topic_check_item.building_floor_id
                      ) {
                        // console.warn(floors_items.building_floor_id)
                        // floors_items.check=false
                        let checklists_data_map = await floors_items.building_checklists.map(
                          async (checklists_item, checklists_index) => {
                            if (
                              checklists_item.photos != null &&
                              checklists_item.photos != undefined &&
                              checklists_item.photos.image != null &&
                              checklists_item.photos.image != undefined &&
                              checklists_item.photos.image.length != 0
                            ) {
                              let data = {
                                token: this.props.token,
                                building_id: buildings_item.building_id,
                                building_floor_id:
                                  floors_items.building_floor_id,
                                building_checklist_id:
                                  checklists_item.building_checklist_id,
                                checklists_item
                              }
                              this.props.sendimage(data)
                              console.log("checklists_item", data)
                            }
                          }
                        )
                      }
                    }
                  )
                }
              }
            )
            // console.warn(topic_check_item)
          }
        }
      )
    })
  }

  sendtoserver1 = async () => {
    let Main_data = this.props.Mainwork
    let value = await AsyncStorage.getItem("PIC_DATA")
    let JSONvalue = await JSON.parse(value)
    let newpicdata = JSONvalue
    // console.warn("JSONvalue", JSONvalue)
    await this.setState({ loading: true }, async () => {
      await this.coutallfun()
      if (this.state.contall == 0) {
        this.setState({ loading: false }, () => {
          return
        })
      }
      let topic_check = await this.state.topic.map(
        async (topic_check_item, topic_check_index) => {
          if (topic_check_item.check == true) {
            // console.warn(Main_data)
            let Main_data_map = await Main_data.buildings.map(
              async (buildings_item, buildings_index) => {
                if (buildings_item.building_id == this.props.buildingsid) {
                  // console.warn(buildings_item.building_id)
                  let floors_data_map = await buildings_item.building_floors.map(
                    async (floors_items, floors_index) => {
                      if (
                        floors_items.building_floor_id ==
                        topic_check_item.building_floor_id
                      ) {
                        // console.warn(floors_items.building_floor_id)
                        // floors_items.check=false
                        let checklists_data_map = await floors_items.building_checklists.map(
                          async (checklists_item, checklists_index) => {
                            if (
                              checklists_item.photos != null &&
                              checklists_item.photos != undefined &&
                              checklists_item.photos.image != null &&
                              checklists_item.photos.image != undefined &&
                              checklists_item.photos.image.length != 0
                            ) {
                              let data = {
                                token: this.props.token,
                                building_checklist_id:
                                  checklists_item.building_checklist_id,
                                image:
                                  checklists_item.photos != null &&
                                  checklists_item.photos != undefined &&
                                  checklists_item.photos.image != null &&
                                  checklists_item.photos.image != undefined &&
                                  checklists_item.photos.image.length != 0
                                    ? checklists_item.photos.image
                                    : null,
                                location:
                                  checklists_item.photos != null &&
                                  checklists_item.photos != undefined &&
                                  checklists_item.photos.location != null
                                    ? checklists_item.photos.location
                                    : null
                              }
                              await this.props
                                .saveallimage(data)
                                .then(async code => {
                                  let imagelist = checklists_item.photos.image.map(
                                    async (image_item, image_index) => {
                                      image_item.image.code = code
                                      await this.setState(
                                        { count: this.state.count + 1 },
                                        async () => {
                                          floors_items.server = "2"
                                          console.log(
                                            "floors_items",
                                            floors_items
                                          )
                                          let chekcpic = false
                                          if (JSONvalue != null) {
                                            let pic_data_map = await newpicdata.map(
                                              (pic_item, pic_index) => {
                                                console.warn(
                                                  "pic_item",
                                                  pic_item
                                                )
                                                if (
                                                  pic_item != null &&
                                                  pic_item != undefined &&
                                                  pic_item.buildingsid ==
                                                    buildings_item.building_id &&
                                                  pic_item.floorid ==
                                                    floors_items.building_floor_id &&
                                                  pic_item.itemsid ==
                                                    checklists_item.building_checklist_id
                                                ) {
                                                  let oldimagelist = pic_item.photos.image.map(
                                                    async (
                                                      old_image_item,
                                                      old_image_index
                                                    ) => {
                                                      if (
                                                        old_image_item.pic_id ==
                                                        image_item.pic_id
                                                      ) {
                                                        chekcpic = true
                                                        old_image_item.image.code = code
                                                      }
                                                    }
                                                  )
                                                  Promise.all(
                                                    oldimagelist
                                                  ).then(async () => {
                                                    let PIC_DATA = await JSON.stringify(
                                                      newpicdata
                                                    )
                                                    console.log(
                                                      "newpicdata",
                                                      newpicdata
                                                    )
                                                    await AsyncStorage.setItem(
                                                      "PIC_DATA",
                                                      PIC_DATA
                                                    )
                                                    await this.props.saveMaindata(
                                                      Main_data
                                                    )

                                                    let MAIN_WORK_DATA = JSON.stringify(
                                                      Main_data
                                                    )
                                                    await AsyncStorage.setItem(
                                                      "MAIN_DATA",
                                                      MAIN_WORK_DATA
                                                    )
                                                    if (
                                                      this.state.count ==
                                                      this.state.contall
                                                    ) {
                                                      this.setState({
                                                        count: 0,
                                                        loading: false,
                                                        topic:
                                                          buildings_item.building_floors
                                                      })
                                                    }
                                                  })
                                                }
                                              }
                                            )
                                          }
                                        }
                                      )
                                    }
                                  )
                                })
                            }
                            // let data = {
                            // token: this.props.token,
                            // building_checklist_id:
                            //   checklists_item.building_checklist_id,
                            // image:
                            //   checklists_item.photos != null &&
                            //   checklists_item.photos != undefined &&
                            //   checklists_item.photos.image != null &&
                            //   checklists_item.photos.image != undefined &&
                            //   checklists_item.photos.image.length != 0
                            //     ? checklists_item.photos.image
                            //     : null,
                            // location:
                            //   checklists_item.photos != null &&
                            //   checklists_item.photos != undefined &&
                            //   checklists_item.photos.location != null
                            //     ? checklists_item.photos.location
                            //     : null
                            // }
                            // await this.props
                            //   .saveallimage(data)
                            //   .then(async code => {
                            //     checklists_item.code = code

                            // await this.setState(
                            //   { count: this.state.count + 1 },
                            //   async () => {
                            //         console.warn(
                            //           this.state.count +
                            //             " / " +
                            //             this.state.contall
                            //         )
                            // floors_items.server = true
                            // console.log("floors_items", floors_items)
                            // let chekcpic = false
                            // if (JSONvalue != null) {
                            //   let pic_data_map = await newpicdata.map(
                            //     (pic_item, pic_index) => {
                            //       if (
                            //         pic_item.buildingsid ==
                            //           buildings_item.building_id &&
                            //         pic_item.floorid ==
                            //           floors_items.building_floor_id &&
                            //         pic_item.itemsid ==
                            //           checklists_item.building_checklist_id
                            //       ) {
                            //         // console.warn("chekcpic = true",code)
                            //         chekcpic = true
                            //         pic_item.code = code
                            //         // console.warn("chekcpic = true",pic_item)
                            //       }
                            //     }
                            //   )
                            //           await Promise.all(pic_data_map).then(
                            //             async () => {
                            //               if (chekcpic == false) {
                            //                 let itempic = {
                            //                   buildingsid:
                            //                     buildings_item.building_id,
                            //                   floorid:
                            //                     floors_items.building_floor_id,
                            //                   itemsid:
                            //                     checklists_item.building_checklist_id,
                            //                   photos: {
                            //                     location:
                            //                       checklists_item.photos !=
                            //                         null &&
                            //                       checklists_item.photos !=
                            //                         undefined &&
                            //                       checklists_item.photos
                            //                         .location != null
                            //                         ? checklists_item.photos
                            //                             .location
                            //                         : null,
                            //                     image:
                            //                       checklists_item.photos !=
                            //                         null &&
                            //                       checklists_item.photos !=
                            //                         undefined &&
                            //                       checklists_item.photos
                            //                         .image != null &&
                            //                       checklists_item.photos
                            //                         .image != undefined &&
                            //                       checklists_item.photos.image
                            //                         .length != 0
                            //                         ? checklists_item.photos
                            //                             .image
                            //                         : null
                            //                   },
                            //                   code: code
                            //                 }
                            //                 newpicdata.push(itempic)
                            //                 // console.warn(
                            //                 //   "chekcpic",
                            //                 //   buildings_item.building_id,
                            //                 //   floors_items.building_floor_id,
                            //                 //   checklists_item.building_checklist_id,
                            //                 //   newpicdata
                            //                 // )
                            //                 let PIC_DATA = await JSON.stringify(
                            //                   newpicdata
                            //                 )
                            //                 await AsyncStorage.setItem(
                            //                   "PIC_DATA",
                            //                   PIC_DATA
                            //                 )
                            //               } else {
                            //                 // console.warn(
                            //                 //   "chekcpic",
                            //                 //   buildings_item.building_id,
                            //                 //   floors_items.building_floor_id,
                            //                 //   checklists_item.building_checklist_id,
                            //                 //   newpicdata
                            //                 // )
                            //                 let PIC_DATA = await JSON.stringify(
                            //                   newpicdata
                            //                 )

                            //                 await AsyncStorage.setItem(
                            //                   "PIC_DATA",
                            //                   PIC_DATA
                            //                 )
                            //               }
                            //             }
                            //           )
                            //         }
                            //         await this.props.saveMaindata(Main_data)
                            //         let MAIN_WORK_DATA = JSON.stringify(
                            //           Main_data
                            //         )
                            //         await AsyncStorage.setItem(
                            //           "MAIN_DATA",
                            //           MAIN_WORK_DATA
                            //         )
                            //         if (
                            //           this.state.count == this.state.contall
                            //         ) {
                            //           this.setState({
                            //             count: 0,
                            //             loading: false,
                            //             topic: buildings_item.building_floors
                            //           })
                            //         }
                            //       }
                            //     )
                            //   })
                          }
                        )
                      }
                    }
                  )
                }
              }
            )
            // console.warn(topic_check_item)
          }
        }
      )
    })
  }

  coutallfun = async () => {
    let contall = 0
    let coutallfun = await this.state.topic.map(
      async (countitem, topicindex) => {
        if (
          countitem.check == true &&
          countitem.building_checklists.length != 0
        ) {
          await countitem.building_checklists.map(
            async (checklists_item, checklists_index) => {
              if (
                checklists_item.photos != null &&
                checklists_item.photos != undefined &&
                checklists_item.photos.image != null &&
                checklists_item.photos.image != undefined &&
                checklists_item.photos.image.length != 0
              ) {
                // console.warn(
                //   "contalllength",
                //   checklists_item.photos.image
                // )
                checklists_item.photos.image.map((item, index) => {
                  if (item.image != "etc" && item.image.code != "0x0000") {
                    console.warn(item)
                    contall += 1
                  }
                })
              }
            }
          )
        }
      }
    )
    await Promise.all(coutallfun)
    await this.setState({ contall: contall }, () => {
      // console.warn("contall", this.state.contall)
    })
  }
  sendtoserver2 = async () => {
    // console.warn("baka")
    let newtopic = this.props.Mainwork
    let value = await AsyncStorage.getItem("PIC_DATA")
    let JSONvalue = await JSON.parse(value)
    let contall = 0
    let newpicdata = JSONvalue
    this.setState({ loading: true }, async () => {
      let topicmapcount = this.state.topic.map(
        async (countitem, topicindex) => {
          if (
            countitem.check == true &&
            countitem.building_checklists.length != 0
          ) {
            contall += countitem.building_checklists.length
          }
        }
      )
      Promise.all(topicmapcount).then(result => {
        // console.warn("Promise")
        this.setState(
          {
            contall: contall
          },
          () => {
            let topicmap = this.state.topic.map(
              async (topicitem, topicindex) => {
                console.log(
                  "topicitem",
                  topicitem.building_floor_id,
                  topicitem.check
                )
                if (
                  topicitem.check == true &&
                  topicitem.building_checklists.length != 0
                ) {
                  // contall =  contall+topicitem.building_checklists

                  let floorid = topicitem.building_floor_id
                  let buildings = await newtopic.buildings.map(
                    async (buildings, buildingsindex) => {
                      if (
                        buildings.building_id == this.props.buildingsid &&
                        buildings.building_floors.length != 0
                      ) {
                        let floorslist = await buildings.building_floors.map(
                          async (floors, floorsindex) => {
                            floors.check = false
                            if (
                              floors.building_floor_id == floorid &&
                              floors.building_checklists.length != 0
                            ) {
                              let checklist = await floors.building_checklists.map(
                                async (checklists, checklistsindex) => {
                                  // console.warn(
                                  //   "id_buliding",
                                  //   buildings.building_id,
                                  //   floors.building_floor_id,
                                  //   checklists.building_checklist_id
                                  // )
                                  let data = {
                                    token: this.props.token,
                                    building_checklist_id:
                                      checklists.building_checklist_id,
                                    image:
                                      checklists.photos != null &&
                                      checklists.photos != undefined &&
                                      checklists.photos.image != null &&
                                      checklists.photos.image != undefined &&
                                      checklists.photos.image.length != 0
                                        ? checklists.photos.image
                                        : null,
                                    location:
                                      checklists.photos != null &&
                                      checklists.photos != undefined &&
                                      checklists.photos.location != null
                                        ? checklists.photos.location
                                        : null
                                  }

                                  // console.log("save data", )
                                  await this.props
                                    .saveallimage(data)
                                    .then(async code => {
                                      // console.log("code", code)
                                      checklists.code = code
                                      if (newpicdata == null) {
                                        newpicdata = [
                                          {
                                            buildingsid: buildings.building_id,
                                            floorid: floors.building_floor_id,
                                            itemsid:
                                              checklists.building_checklist_id,
                                            photos: {
                                              location:
                                                checklists.photos != null &&
                                                checklists.photos !=
                                                  undefined &&
                                                checklists.photos.location !=
                                                  null
                                                  ? checklists.photos.location
                                                  : null,
                                              image:
                                                checklists.photos != null &&
                                                checklists.photos !=
                                                  undefined &&
                                                checklists.photos.image
                                                  .length != 0
                                                  ? checklists.photos.image
                                                  : null
                                            },
                                            code: code
                                          }
                                        ]
                                      } else {
                                        let checkpic = false
                                        let newpicarry = await newpicdata.map(
                                          async (picdata, picdataindex) => {
                                            if (
                                              picdata.buildingsid ==
                                                buildings.building_id &&
                                              picdata.floorid ==
                                                floors.building_floor_id &&
                                              picdata.itemsid ==
                                                checklists.building_checklist_id
                                            ) {
                                              picdata.code = code
                                              checkpic = true
                                            }
                                          }
                                        )
                                        await Promise.all(newpicarry).then(
                                          async result => {
                                            if (checkpic == true) {
                                              let itempic = {
                                                buildingsid:
                                                  buildings.building_id,
                                                floorid:
                                                  floors.building_floor_id,
                                                itemsid:
                                                  checklists.building_checklist_id,
                                                photos: {
                                                  location:
                                                    checklists.photos != null &&
                                                    checklists.photos !=
                                                      undefined &&
                                                    checklists.photos
                                                      .location != null
                                                      ? checklists.photos
                                                          .location
                                                      : null,
                                                  image:
                                                    checklists.photos != null &&
                                                    checklists.photos !=
                                                      undefined &&
                                                    checklists.photos.image !=
                                                      null &&
                                                    checklists.photos.image !=
                                                      undefined &&
                                                    checklists.photos.image
                                                      .length != 0
                                                      ? checklists.photos.image
                                                      : null
                                                },
                                                code: code
                                              }
                                              newpicdata.push(itempic)
                                            }
                                            let PIC_DATA = JSON.stringify(
                                              newpicdata
                                            )
                                            await AsyncStorage.setItem(
                                              "PIC_DATA",
                                              PIC_DATA
                                            )
                                          }
                                        )
                                      }
                                    })

                                  let MAIN_WORK_DATA = JSON.stringify(newtopic)
                                  await AsyncStorage.setItem(
                                    "MAIN_DATA",
                                    MAIN_WORK_DATA
                                  )
                                  this.setState(
                                    { count: this.state.count + 1 },
                                    async () => {
                                      await this.props.saveMaindata(newtopic)
                                    }
                                  )
                                }
                              )
                              await Promise.all(checklist)
                            }
                          }
                        )
                        await Promise.all(floorslist)
                      }
                    }
                  )
                  await Promise.all(buildings)
                }
              }
            )
            this.promiselast(topicmap)
          }
        )
      })
    })
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    work: state.work.work,
    topic: state.topic.topic,
    topicerror: state.topic.topicerror,
    image: state.topic.image,
    imageerror: state.topic.imageerror,
    Mainwork: state.main.Allwork,
    error: state.main.error,
    random: state.main.random,
    checkerror: state.topic.checkerror,
    photocount: state.topic.photocount,
    isLast: state.topic.isLast
  }
}

export default connect(
  mapStateToProps,
  actions
)(Topiclist)
