import React, { Component } from "react"
import {
  Platform,
  AsyncStorage,
  Text,
  View,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Dimensions,
  Alert
} from "react-native"

import * as actions from "../Actions"
import { SUBCOLOR1 } from "../Constants/Color"
import styles from "./styles/login.style"

import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import LinearGradient from "react-native-linear-gradient"
import DeviceInfo from "react-native-device-info"

const logo = require("../../assets/image/logo-BBI.png")
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width
class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      text: [],
      loading: true,
      Username: null,
      Password: null,
      random_login: null
    }
  }

  async componentDidMount() {
    // if(__DEV__){
    //   let data = {
    //     username:"au01",
    //     password: "1234"
    //   }
    //   this.props.loginfetch(data)
    // }

    let value = await AsyncStorage.getItem("USER_DATA")
    if (value != null) {
      let JSONvalue = JSON.parse(value)
      // console.warn(JSONvalue)
      this.props.loginfetch({
        username: JSONvalue.username,
        password: JSONvalue.password
      })
    } else {
      this.setState({ loading: false })
    }
  }

  onPress = () => {
    if (this.state.Username == null) {
      Alert.alert("Error", "กรุณากรอกชื่อผู้ใช้งาน", [
        { text: "OK", onPress: () => console.log("OK Pressed out") }
      ])
      return
    }
    if (this.state.Password == null) {
      Alert.alert("Error", "กรุณากรอกรหัสผ่าน", [
        { text: "OK", onPress: () => console.log("OK Pressed out") }
      ])
      return
    }
    this.setState({ loading: true }, () => {
      this.props.loginfetch({
        username: this.state.Username,
        password: this.state.Password
      })
    })

    // Actions.worklist()
  }

  async componentWillReceiveProps(nextprops) {
    if (nextprops.token != null) {
      this.setState({ loading: false })
      Actions.worklist()
    }

    if (nextprops.error != null) {
      let value = await AsyncStorage.getItem("USER_DATA")
      if (value != null) {
        let JSONvalue = JSON.parse(value)
        this.setState({ loading: false,randomlogin:nextprops.randomlogin },()=>{
          Actions.worklist()
        })
        
        
      }else{
        this.setState({ loading: false,randomlogin:nextprops.randomlogin },()=>{
          Alert.alert("Error", nextprops.error, [
            { text: "OK", onPress: () => console.log("OK Pressed out") }
          ])
        })
        
      }
    }
  }

  render() {
    if (this.state.loading == true) {
      return (
        <View style={[styles.container, { justifyContent: "center" }]}>
          <LinearGradient
            colors={["#DFC174", "#443409"]}
            style={[styles.linearGradient,{ justifyContent: "center" }]}
          >
            <ActivityIndicator size="large" color={SUBCOLOR1} />
          </LinearGradient>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <LinearGradient
          colors={["#DFC174", "#443409"]}
          style={styles.linearGradient}
        >
          <KeyboardAvoidingView style={styles.textinputbox}>
            <Image source={logo} style={[styles.image, {}]} />
            <TextInput
              style={styles.textinput}
              value={this.state.Username}
              placeholder="Username"
              onChangeText={Username => {
                this.setState({ Username })
              }}
              onSubmitEditing={event => this.refs.pass.focus()}
            />
            <TextInput
              style={[styles.textinput, { marginTop: 20 }]}
              ref="pass"
              value={this.state.password}
              placeholder="Password"
              onChangeText={Password => {
                this.setState({ Password })
              }}
              onSubmitEditing={this.onPress}
              secureTextEntry={true}
            />
            <TouchableOpacity style={styles.button} onPress={this.onPress}>
              <Text>{"Login"}</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
          <View style={styles.textversionbox}>
            <Text style={styles.textversion}>
              {"V "}
              {DeviceInfo.getVersion()}
            </Text>
          </View>
        </LinearGradient>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    error: state.auth.error,
    randomlogin: state.auth.randomlogin
  }
}

export default connect(
  mapStateToProps,
  actions
)(Login)
