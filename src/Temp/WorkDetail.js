import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Dimensions,
  WebView,
  ActivityIndicator,
  ScrollView
} from "react-native"

import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { Icon } from "react-native-elements"
import LinearGradient from "react-native-linear-gradient"
import HTML from "react-native-render-html"
import { MAIN_COLOR, BLCK_COLOR } from "../Constants/Color"
import styles from "./styles/Workdetail.style"
import * as actions from "../Actions"

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default class WorkDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: false,
      loading: false
    }
  }

  componentDidMount() {
   
  }


  render() {
    if (this.state.loading == true) {
      return (
        <View style={[styles.container, {}]}>
          <ActivityIndicator size="large" color={MAIN_COLOR} />
        </View>
      )
    }
    return (
      <View style={[styles.container, {}]}>
        <ScrollView>
          <View style={[styles.titlebox, {}]}>
            <Text style={[styles.title]}>{this.props.data.name}</Text>
          </View>
          <HTML
            html={this.props.data.description}
            imagesMaxWidth={Dimensions.get("window").width}
            containerStyle={{
              width: (width / 100) * 90,
              alignSelf: "center",
              margin: 10
            }}
          />
        </ScrollView>
      </View>
    )
  }
}
