import { Platform, StyleSheet, Dimensions } from "react-native"
import { WHITE_COLOR, SUBCOLOR1 } from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  cameraRollList: {
    width: (Dimensions.get("window").width - 50) / 3,
    height: (Dimensions.get("window").width - 50) / 3,
    backgroundColor: "transparent",
    borderWidth: 1,
    borderColor: "#bababa",
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10
    // backgroundColor: "blue"
  },
  buttonAdd: {
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "red"
  },
  cameraRollRow: {
    maxWidth: "100%",
    flex: 1,
    minWidth: Dimensions.get("window").width - 20,
    marginTop: 80,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 80
    // backgroundColor: "pink"
  },
  navigation: {
    paddingTop: Platform.OS === "ios" ? 25 : 10,
    backgroundColor: "transparent",
    width: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1
  },
  navigationBlue: {
    backgroundColor: SUBCOLOR1,
    borderBottomWidth: 1,
    borderColor: "#848789"
  },
  navigationTitle: {
    flexDirection: "row",
    justifyContent: "center",
    minHeight: 30
  },
  navigationTitleText: {
    color: "#fff",
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  buttonFixed: {
    flexDirection: "column",
    flex: 1,
    position: "absolute",
    bottom: 0
  },
  second: {
    height: 50,
    flexDirection: "row"
  },
  firstButton: {
    width: "100%",
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  secondButton: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: SUBCOLOR1,
  },
  selectButton: {
    color: "#FFF",
    fontSize: 20
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  buttonRight: {
    position: 'absolute',
    top: 0,
    right: 20,
    width: Dimensions.get("window").width /100 * 8,
    height:  Dimensions.get("window").width /100 * 8,
    // backgroundColor:'red'
  },
  buttonDelete: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 1
  },
  buttoncheck:{
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 1
  },
  cameraRollImgBorderGallary: {
    width: (Dimensions.get('window').width-50)/3,
    height: (Dimensions.get('window').width-50)/3,
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#bababa',
  },
  cameraRollImg: {
    width: (Dimensions.get('window').width-50)/3,
    height: (Dimensions.get('window').width-50)/3
  },
  cameraRollImgBorder: {
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#bababa'
  }
})
