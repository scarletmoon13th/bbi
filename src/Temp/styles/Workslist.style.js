import { Platform, StyleSheet, Dimensions } from "react-native"
import {
  WHITE_COLOR,
  BASIC_COLOR,
  SUBCOLOR1,
  BGCOLORWORK
} from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BGCOLORWORK
  },
  Itemsbox: {
    justifyContent: "center",
    alignSelf: "center",
    // backgroundColor: "red",
    width: (width / 100) * 90,
    height: (height / 100) * 10,
    flexDirection: "row",
    borderWidth: 1,
    margin: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderRadius: (Dimensions.get("window").width / 100) * 3,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    backgroundColor: WHITE_COLOR
  },
  Innerstartsbox: {
    width: "20%",
    height: "100%",
    // backgroundColor: "red",
    paddingVertical: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  Innermiddlebox: {
    width: "80%",
    height: "100%",
    // backgroundColor: "blue",
    justifyContent: "center",
    paddingVertical: 10,
    paddingLeft: 25
  },
  Innerendbox: {
    width: "20%",
    height: "100%",
    // backgroundColor: "yellow",
    // borderWidth: 1,
    // borderRadius: 100 / 2,
    justifyContent: "center",
    paddingVertical: 10
  },
  linearGradient: {
    flex: 1
  },
  navigation: {
    paddingTop: Platform.OS === "ios" ? 25 : 10,
    backgroundColor: "transparent",
    justifyContent: "center",
    width: "100%",
    flexDirection: "row",
    alignContent: "center",
    // borderWidth: 1,
    // borderColor: "#fff"
    // position: "absolute",
  },
  navigationBlue: {
    backgroundColor: SUBCOLOR1,
    borderBottomWidth: 1,
    borderColor: "#848789"
  },
  navigationTitle: {
    // flexDirection: "row",

    minHeight: 30
  },
  navigationTitleText: {
    color: "#fff",
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  navigationLogout: {
    // width: 20,
    height: '100%',
    // backgroundColor: "red",
    position:'absolute',
    alignSelf:'center',
    right:20,
    alignContent: "center",
    justifyContent: "center"
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  buttonRight: {
    position: "absolute",
    top: 0,
    right: 20
  },
  boxRow: {
    width: width

    // maxWidth: "100%",
    // flex: 1,
    // minWidth: Dimensions.get("window").width - 20,
    // marginTop: 80,
    // paddingLeft: 10,
    // paddingRight: 10,
    // marginBottom: 80
    // backgroundColor: "pink"
  }
})
