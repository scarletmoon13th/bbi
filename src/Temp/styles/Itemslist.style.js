import { Platform, StyleSheet, Dimensions } from "react-native"
import { SUBCOLOR1, BLCK_COLOR, LIGHT_BLCK_COLOR ,WHITE_COLOR} from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  navigation: {
    paddingTop: Platform.OS === "ios" ? 25 : 10,
    backgroundColor: "transparent",
    width: "100%",
    height:height/100*7,
    // position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1
  },
  navigationBlue: {
    backgroundColor: SUBCOLOR1,
    borderBottomWidth: 1,
    borderColor: "#848789"
  },
  navigationTitle: {
    flexDirection: "row",
    justifyContent: "center",
    minHeight: 30
  },
  navigationTitleText: {
    color: "#fff",
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  buttonRight: {
    position: "absolute",
    top: 0,
    left: 0,
    width:width/100*15,
    height:'100%'
  },
  bodybox:{
    width:width,
    height:height/100*91.5,
    // alignItems: "center",
    // justifyContent: "center",
    // flex:1,
    // borderWidth:1,
    // borderColor:'red'
    // backgroundColor:'#000222'
  },
  titlebox: {
    width: (width / 100) * 40,
    // height:height/100*10,
    borderWidth: 0.5,
    margin: 10,
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 10,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: WHITE_COLOR,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    flexDirection: "row"
  },
  text: {
    color: BLCK_COLOR
  },
  Toptitlebox: {
    width: "100%",
    marginLeft: "5%",
    // borderBottomWidth: 0.5
    // alignItems: "flex-end",
  },
  endtitlebox: {
    width: "100%",
    alignItems: "flex-end"
  },
  titleinnerbox:{
    width: "90%",
     alignItems: "center",
    justifyContent: "center",
  }
})