import { Platform, StyleSheet, Dimensions } from "react-native"
import { WHITE_COLOR, BLCK_COLOR, LIGHT_BLCK_COLOR ,MAIN_COLOR} from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR,
    // alignContent: "center",
    // justifyContent: "center"
  },
  textinputbox: {
    width: width,
    // borderWidth: 1,
    // padding: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  textinput: {
    width: (width / 100) * 80,
    // borderWidth: 1,
    borderRadius: 10,
    paddingLeft: 10,
    backgroundColor:WHITE_COLOR,
    opacity:0.5,
    textAlign:"center",
    // color:WHITE_COLOR
    // alignContent: "center",
    // justifyContent: "center"
  },
  button: {
    width: (width / 100) * 80,
    borderWidth: 0.2,
    alignItems: "center",
    justifyContent: "center",
    marginVertical:10,
    padding:10,
    marginTop:20,
    borderRadius: 10,
    backgroundColor: MAIN_COLOR,
  },
  image:{
    width:width/100*60,
    height:height/100*20,
    resizeMode:"contain",
    marginBottom:40,
    marginTop:20
  },
  linearGradient: {
    flex: 1,
    // borderWidth: 10,
    // paddingLeft: 15,
    // paddingRight: 15,
    // borderRadius: 5
  },
  textversion:{
    color:WHITE_COLOR,
    // alignItems: "flex-end",
    // justifyContent: "flex-end",
    // justifyContent:"flex-end",
    // margin:10,
    // position:"absolute"
    
  },
  textversionbox:{
    position: "absolute", 
    bottom: 0, 
    right: 0
  }
})
