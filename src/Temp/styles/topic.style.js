import { Platform, StyleSheet, Dimensions } from "react-native"
import {
  WHITE_COLOR,
  SUBCOLOR1,
  BASIC_COLOR,
  BLCK_COLOR,
  BGCOLOR,
  BUTTON_COLOR
} from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BGCOLOR
  },
  titlebox: {
    width: (width / 100) * 40,
    // height:height/100*10,
    borderWidth: 0.5,
    margin: 10,
    paddingVertical: 10,
    paddingHorizontal: 5,
    borderRadius: 10,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: WHITE_COLOR,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    flexDirection: "row"
  },
  checkbox: {
    width: "15%",
    // height:"100%",
    marginLeft: "4%",
    alignItems: "center",
    justifyContent: "center"
    // borderWidth: 1,
  },
  linearGradient: {
    flex: 1
  },
  titleinnerbox: {
    width: "75%",
    marginLeft: "5%",
    // borderWidth: 1,
    borderLeftWidth: 0.5
  },
  Toptitlebox: {
    width: "100%",
    marginLeft: "5%",
    borderBottomWidth: 0.5
    // alignItems: "flex-end",
  },
  endtitlebox: {
    width: "100%",
    alignContent: "center",
    justifyContent:'flex-end',
    flexDirection:'row'
  },
  text: {
    color: BLCK_COLOR
  },
  serv:{
    width:width/360*10,
    height:width/360*10,
    backgroundColor:'red',
    borderRadius:50/2,
    alignSelf:'center'
  },
  navigation: {
    paddingTop: Platform.OS === "ios" ? 25 : 10,
    backgroundColor: "transparent",
    width: "100%"
    // position: "absolute",
    // top: 0,
    // left: 0,
    // zIndex: 1
  },
  navigationBlue: {
    backgroundColor: SUBCOLOR1,
    borderBottomWidth: 1,
    borderColor: "#848789"
  },
  navigationTitle: {
    flexDirection: "row",
    justifyContent: "center",
    minHeight: 30
  },
  navigationTitleText: {
    color: "#fff",
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  buttonLeftResize: {
    width: 25,
    height: 25
  },
  buttonRight: {
    position: "absolute",
    top: 0,
    left: 0,
    width:width/100*15,
    height:'100%'
  },
  boxRow: {
    // maxWidth: "100%",
    // height: (height / 100) * 75,
    flex: 1,
    // minWidth: Dimensions.get("window").width - 20,
    // marginTop: 80,
    // paddingLeft: 10,
    // paddingRight: 10,
    // marginBottom: 80,
    alignSelf: "center",
    // backgroundColor: "pink"
    // borderWidth: 1
  },
  selectallbox: {
    width: (width / 100) * 90,
    height: (height / 100) * 5,
    paddingTop: 10,
    paddingBottom: 5,
    // marginLeft:10,
    flexDirection: "row"
  },
  submitbut:{
    width: (width / 100) * 100,
    height: (height / 100) * 10,
    backgroundColor:BUTTON_COLOR,
    justifyContent: "center",
    alignItems: "center",
  },
  submittext:{
    color:WHITE_COLOR,
    fontSize:width/360*22
  }
})
