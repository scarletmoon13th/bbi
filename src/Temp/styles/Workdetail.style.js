import { Platform, StyleSheet, Dimensions } from "react-native"
import {
  WHITE_COLOR,
  BASIC_COLOR,
  SUBCOLOR1,
  BGCOLORWORK
} from "../../Constants/Color"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  titlebox: {
    width: (width / 100) * 80,
    borderBottomWidth: 1,
    alignSelf: "center",
    justifyContent: "center",
    padding: 10,
    margin: 10
  },
  title: {
    fontSize: (width / 360) * 18
  }
})
