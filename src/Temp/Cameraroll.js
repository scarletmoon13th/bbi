import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Alert,
  Dimensions,
  FlatList,
  Slider,
  CameraRoll,
  PermissionsAndroid,
  BackHandler
} from "react-native"

import ModalSelector from "react-native-modal-selector"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { Icon } from "react-native-elements"
import RNFS from "react-native-fs"
import RNFetchBlob from "rn-fetch-blob"
import ImageZoom from "react-native-image-pan-zoom"
import moment from "moment"
import DeviceInfo from "react-native-device-info"
// import FusedLocation from "react-native-fused-location"

import { WHITE_COLOR, SUBCOLOR1, MAIN_COLOR } from "../Constants/Color"
import styles from "./styles/Cameraroll.style"
import Camera, { RNCamera } from "react-native-camera"
const height = Dimensions.get("window").height
const width = Dimensions.get("window").width
class Cameraroll extends Component {
  constructor(props) {
    super(props)
    _isMounted = false
    this.state = {
      photos: [],
      display: "",
      type: RNCamera.Constants.Type.back,
      flash: false,
      focus: true,
      zoom: 0,
      location: null,
      newphoto: [],
      removedarr: [],
      photosLib: [],
      loading: false,
      previewimage: null,
      widthphoto: null,
      heightphoto: null,
      newphto: null
    }
  }
  async componentDidMount() {
    this._isMounted = true

    if (this._isMounted == true) {
      this.getPhotosLib()
      this.requestCameraPermission()
      this.saveLocation()
      this.backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          this.goBack() // works best when the goBack is async
          return true
        }
      )
      var photos = []

      if (this._isMounted == true && this.props.title != null) {
        // photos.push({ image: "etc" })
        if (
          this._isMounted == true &&
          this.props.image != null &&
          this.props.image.building_checklist_result.length != 0
        ) {
          // await this.props.image.building_checklist_result.map(item => {
          //   console.log("photos", this._isMounted)
          //   if (this._isMounted == true) {
          //     // console.log(Actions.currentScene)
          //     photos.push({ image: item })
          //   }
          // })
        }
      }

      if (this._isMounted == true) {
        // console.log(Actions.currentScene)
        this.setState({ photos }, () => {
          // console.log(photos)
        })
      }
    }

    if (
      this.props.buildingsid != null &&
      this.props.Mainwork.buildings != null &&
      this.props.floorid != null &&
      this._isMounted == true
    ) {
      await this.props.Mainwork.buildings.map(async (item, index) => {
        if (item.building_id == this.props.buildingsid) {
          await item.building_floors.map(async (item, index) => {
            if (item.building_floor_id == this.props.floorid) {
              await item.building_checklists.map(async (item, index) => {
                if (
                  item.building_checklist_id == this.props.itemsid &&
                  item.photos != null &&
                  item.photos != undefined &&
                  item.photos.image != null &&
                  item.photos.image != undefined &&
                  item.photos.image.length != 0
                ) {
                  let image = item.photos.image
                  // console.log("image", item.photos.image)
                  if (item.photos.image[0].image != "etc") {
                    image.unshift({ image: "etc" })
                  }

                  this.setState({ photos: image })

                  // item.photos.image.unshift({ image: "etc" })
                  // item.photos.image.map((item,index)=>{

                  // this.state.photos.push({ image: item })
                  // console.log(this.state.photos)
                  // })
                  // item.photos = {location:this.state.location,image:this.state.photos}
                } else if (
                  item.building_checklist_id == this.props.itemsid &&
                  (item.photos == null ||
                    item.photos == undefined ||
                    item.photos.image == null ||
                    item.photos.image == undefined ||
                    item.photos.image.length == 0)
                ) {
                  if (this.state.photos.length == 0) {
                    this.setState({ photos: [{ image: "etc" }] })
                  }
                }
              })
            }
          })
        }
      })
    }
  }

  componentWillUnmount() {
    this._isMounted = false

    // FusedLocation.off(this.subscription)
    // FusedLocation.off(this.errSubscription);
    // FusedLocation.stopLocationUpdates()
  }
  goBack = () => {
    if (this.state.display == "camera") {
      this.setState({ display: "" }, async () => {
        await this.saveallpic()
      })
    }else if(this.state.display == ""){
      this.onBackButton()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.check != null) {
      // Actions.pop({ data: "refresh" })
      this.setState({ loading: false }, () => {
        Actions.pop({ refresh: { data: "refresh" }, timeout: 1 })
      })
    }
    if (nextProps.checkerror != null) {
      Alert.alert("ERROR!", nextProps.checkerror, [
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ])
    }
  }

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "Cool Photo App Camera Permission",
          message:
            "Cool Photo App needs access to your camera " +
            "so you can take awesome pictures."
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log("You can use the camera", this._isMounted)
        // console.log(Actions.currentScene)
      } else {
        // console.log("Camera permission denied")
      }
    } catch (err) {}
  }

  async saveLocation() {
    // console.log(Actions.currentScene)
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "App needs to access your location",
        message:
          "App needs access to your location " +
          "so we can let our app be even more awesome."
      }
    )
    if (granted) {
      // FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY)
      // const location = await FusedLocation.getFusedLocation()
      let location = await geolocation.getCurrentPosition([{enableHighAccuracy:true}]);
      let lat = location.latitude
      let log = location.longitude
      let data = {
        position: {
          lat: lat,
          log: log
        }
      }
      // let data = {
      //     position: {
      //       lat: 1,
      //       log: 1
      //     }
      //   }
      // console.log(Actions.currentScene)
      this.setState({ location: data }, () => {})
    } else {
    }
  }

  onBackButton() {
    // this.setState({photos:[]})
    Actions.pop()
    return true
  }

  getPhotosLib() {
    // this.setState({photosLib:[]});
    CameraRoll.getPhotos({ first: 999, assetType: "Photos" })
      .then(r => {
        for (var i = 0; i < r.edges.length; i++) {
          this.state.photosLib.push({
            image: r.edges[i].node.image.uri,
            timestamp: r.edges[i].node.timestamp
          })
        }
        // console.log("photosLib", this.state.photosLib)
      })
      .catch(error => {
        console.error(error)
      })
  }

  saveallpic = async () => {
    this.setState({ loading: true }, async () => {
      let data = {
        token: this.props.token,
        building_checklist_id: this.props.building_checklist_id,
        image: this.state.photos,
        location: this.state.location
        // deleteimage: this.state.removedarr,
      }
      let newsavephoto = this.props.Mainwork
      let photo = this.state.photos
      let olddata = []
      let value = await AsyncStorage.getItem("PIC_DATA")
      let JSONvalue = JSON.parse(value)
      olddata = JSONvalue
      let newdata = null
      photo.splice(0, 1)

      let newsavephoto_list = await newsavephoto.buildings.map(
        async (item, index) => {
          if (item.building_id == this.props.buildingsid) {
            // console.log(item.building_id)
            await item.building_floors.map(async (item, index) => {
              // console.log(item.building_floor_id, this.props.floorid)
              if (item.building_floor_id == this.props.floorid) {
                await item.building_checklists.map(async (item, index) => {
                  // console.log(item.building_checklist_id, this.props.itemsid)
                  if (item.building_checklist_id == this.props.itemsid) {
                    item.photos = {
                      location: this.state.location,
                      image: photo
                    }
                    console.log("picdata", item)
                    if (olddata == null) {
                      newdata = [
                        {
                          buildingsid: this.props.buildingsid,
                          floorid: this.props.floorid,
                          itemsid: this.props.itemsid,
                          photos: {
                            location: this.state.location,
                            image: photo
                          }
                        }
                      ]

                      let PIC_DATA = JSON.stringify(newdata)
                      // console.log("PIC_DATA", PIC_DATA)
                      await AsyncStorage.setItem("PIC_DATA", PIC_DATA)
                    } else {
                      let checkpic = false
                      olddata.map(async (picdata, picdataindex) => {
                        // console.warn("picdata.buildingsid ",picdata.buildingsid)
                        if (
                          picdata != null &&
                          picdata.buildingsid != null &&
                          picdata.buildingsid == this.props.buildingsid &&
                          picdata.floorid == this.props.floorid &&
                          picdata.itemsid == this.props.itemsid
                        ) {
                          picdata.photos = {
                            location: this.state.location,
                            image: photo
                          }
                          checkpic = true
                        }
                      })
                      Promise.all(olddata).then(async result => {
                        if (checkpic != true) {
                          newdata = {
                            buildingsid: this.props.buildingsid,
                            floorid: this.props.floorid,
                            itemsid: this.props.itemsid,
                            photos: {
                              location: this.state.location,
                              image: photo
                            }
                          }
                          if (newdata !== null) {
                            olddata.push(newdata)
                            let PIC_DATA = JSON.stringify(olddata)
                            await AsyncStorage.setItem("PIC_DATA", PIC_DATA)
                          }
                        } else {
                          let PIC_DATA = JSON.stringify(olddata)
                          await AsyncStorage.setItem("PIC_DATA", PIC_DATA)
                        }
                      })

                      // console.log(PIC_DATA)
                    }
                  }
                })
              }
            })
          }
        }
      )
      Promise.all(newsavephoto_list).then(async () => {
        if (
          this.state.photos[0] != null &&
          this.state.photos[0].image != "etc"
        ) {
          await this.state.photos.unshift({ image: "etc" })
        }

        let MAIN_WORK_DATA = await JSON.stringify(newsavephoto)
        await AsyncStorage.setItem("MAIN_DATA", MAIN_WORK_DATA)
        await this.props.saveMaindata(newsavephoto)
        setTimeout(() => {
          this.setState({ loading: false })
        }, 1000)

        // Actions.pop()
      })
    })
  }

  renderlist() {
    if (this.state.loading == true) {
      return (
        <View style={[styles.container, { justifyContent: "center" }]}>
          <ActivityIndicator size="large" color={SUBCOLOR1} />
        </View>
      )
    }
    return (
      <View style={[styles.container, {}]}>
        <View style={[styles.navigation, styles.navigationBlue]}>
          <View style={styles.navigationTitle}>
            <Text style={styles.navigationTitleText}>{this.props.title}</Text>
          </View>
        </View>
        <FlatList
          data={this.state.photos}
          renderItem={this.renderItem}
          numColumns={3}
          keyExtractor={item => item.image}
          keyboardDismissMode="on-drag"
          style={styles.cameraRollRow}
        />
        <View style={styles.buttonFixed}>
          <View style={[styles.second]}>
            <TouchableOpacity
              style={styles.firstButton}
              onPress={this.onBackButton.bind(this)}
            >
              <Text style={styles.selectButton}>{"ปิด"}</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
              style={[styles.secondButton]}
              onPress={() => {
                this.saveallpic()
              }}
            >
              <Text style={styles.selectButton}>{"บันทึก"}</Text>
            </TouchableOpacity>*/}
          </View>
        </View>
      </View>
    )
  }
  _keyExtractor = (item, index) => item.key
  renderItem = (item, index) => {
    const data = [
      {
        key: index++,
        label: "กล้องถ่ายรูป",
        action: "camera"
      },
      {
        key: index++,
        label: "เลือกรูปจากอัลบั้ม",
        action: "photoLibrary"
      }
    ]
    console.warn(item.item)

    switch (item.item.image) {
      case "etc":
        return (
          <TouchableOpacity
            style={[styles.cameraRollList, { justifyContent: "center" }]}
            onPress={() => {
              this.setState({ display: "camera" })
            }}
          >
            <View style={styles.buttonAdd}>
              <Image source={require("../../assets/image/icon_plus.png")} />
            </View>
          </TouchableOpacity>
        )
      default:
        // console.log("default")
        return (
          <View style={styles.cameraRollList}>
            <TouchableOpacity
              style={styles.buttonDelete}
              onPress={() => {
                this.onDeleteItem(item.item.image)
              }}
            >
              <Image
                source={require("../../assets/image/icon_delete.png")}
                onPress={() => {
                  // this.onDeleteItem(item.image)
                }}
              />
            </TouchableOpacity>
            {item.item.image.code != null &&
              item.item.image.code != undefined &&
              item.item.image.code == "0x0000" && (
                <View style={styles.buttoncheck}>
                  <Icon
                    name="database-check"
                    type="material-community"
                    color="#43A047"
                    // style={{
                    //   position: "absolute",
                    //   bottom: 2,
                    //   left: 2,
                    //   alignSelf: "flex-end"
                    // }}
                    size={20}
                  />
                </View>
              )}

            <TouchableOpacity
              style={styles.cameraRollImgBorderGallary}
              onPress={() => {
                Image.getSize(
                  item.item.image.image,
                  (widthphoto, heightphoto) => {
                    this.setState({
                      display: "preview",
                      previewimage: item.item.image.image,
                      widthphoto: widthphoto,
                      heightphoto: heightphoto
                    })
                  }
                )

                // this.getbase64img(item.image)
              }}
            >
              <View style={styles.cameraRollImgBorderGallary} accessible={true}>
                <Image
                  style={styles.cameraRollImg}
                  source={{
                    uri: item.item.image.image
                  }}
                  resizeMode={"cover"}
                />
              </View>
            </TouchableOpacity>
          </View>
        )
    }
  }
  renderCamera() {
    return (
      <View
        style={{
          flex: 1,
          paddingTop: 10,
          backgroundColor: "#000"
        }}
      >
        <View style={styles.navigationTitle}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={[styles.buttonRight]}
            onPress={async () => {
              this.setState({ display: "" }, async () => {
                await this.saveallpic()
              })
            }}
          >
            <Image
              source={require("../../assets/image/icon_cancel.png")}
              style={styles.buttonLeftResize}
            />
          </TouchableOpacity>
        </View>
        <RNCamera
          ref={cam => {
            this.camera = cam
          }}
          style={{ flex: 1 }}
          // style={styles.cameraButtonPosition}
          // aspect={this.state.camera.aspect}
          // captureTarget={this.state.camera.captureTarget}
          type={this.state.type}
          zoom={this.state.zoom}
          // orientation={this.state.camera.orientation}
          // fixOrientation={true}
          autoFocus={
            this.state.focus
              ? RNCamera.Constants.AutoFocus.on
              : RNCamera.Constants.AutoFocus.off
          }
          // focusDepth={this.state.depth}
          ration="4:3"
          flashMode={
            this.state.flash
              ? RNCamera.Constants.FlashMode.on
              : RNCamera.Constants.FlashMode.off
          }
          playSoundOnCapture={true}
          // captureQuality="medium"
        />
        <View style={{ alignItems: "center", flexDirection: "row" }}>
          <View style={{ width: "20%", alignItems: "center" }}>
            <Slider
              style={{
                width: "100%",
                alignSelf: "flex-end",
                backgroundColor: "#FFF"
              }}
              onValueChange={zoom => this.setState({ zoom })}
              step={0.1}
            />
          </View>
          <View style={{ width: "20%" }}>
            {this.state.flash ? (
              <Icon
                reverse
                raised
                name="flash"
                type="material-community"
                color="#007CC2"
                size={20}
                onPress={() => this.setState({ flash: !this.state.flash })}
              />
            ) : (
              <Icon
                reverse
                raised
                name="flash-off"
                type="material-community"
                color="#007CC2"
                size={20}
                onPress={() => this.setState({ flash: !this.state.flash })}
              />
            )}
          </View>
          <View style={{ width: "30%" }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                this.onPhoto()
              }}
            >
              <Image source={require("../../assets/image/icon_snap.png")} />
            </TouchableOpacity>
          </View>

          <View style={{ width: "20%" }}>
            <TouchableOpacity
              style={{
                width: (width / 100) * 15,
                height: (width / 100) * 15
              }}
            >
              <Image
                style={{ width: "100%", height: "100%" }}
                source={{ uri: this.state.newphto }}
                // resizeMode={"cover"}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
  renderPhotoLibrary() {
    let index = 0
    const data = [
      {
        key: index++,
        label: "Take Photo",
        action: "camera"
      },
      {
        key: index++,
        label: "Choose from Library",
        action: "photoLibrary"
      }
    ]
    // console.log(this.state.photosLib)
    return (
      <View style={styles.container}>
        <View style={[styles.navigation, styles.navigationBlue]}>
          <View style={styles.navigationTitle}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.buttonRight}
              onPress={() => {
                this.setState({ display: "" })
              }}
            >
              <Image
                source={require("../../assets/image/icon_cancel.png")}
                style={styles.buttonLeftResize}
              />
            </TouchableOpacity>
            <Text style={styles.navigationTitleText}>
              {/*this.props.title*/}
            </Text>
          </View>
        </View>
        <FlatList
          style={styles.cameraRollRow}
          data={this.state.photosLib}
          numColumns={3}
          keyExtractor={item => item.image}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  this.onlibphoto(item.image, "gallery", item.timestamp)
                }}
              >
                <View style={styles.cameraRollImgBorder}>
                  <Image
                    style={styles.cameraRollImg}
                    source={{
                      uri: item.image
                    }}
                    resizeMode={"cover"}
                  />
                </View>
              </TouchableOpacity>
            )
          }}
        />
      </View>
    )
  }

  renderPreview() {
    console.warn(
      this.state.previewimage,
      this.state.widthphoto,
      this.state.heightphoto
    )

    return (
      <View
        style={{
          flex: 1,
          paddingTop: 10,
          backgroundColor: "#000"
        }}
      >
        <View style={styles.navigationTitle}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.buttonRight}
            onPress={() =>
              this.setState({
                display: "",
                widthphoto: null,
                heightphoto: null,
                previewimage: null
              })
            }
          >
            <Image
              source={require("../../assets/image/icon_cancel.png")}
              style={styles.buttonLeftResize}
            />
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          {this.state.previewimage != null && (
            <ImageZoom
              cropWidth={width}
              cropHeight={height}
              imageWidth={width}
              imageHeight={height}
              panToMove={"true"}
              pinchToZoom={"true"}
            >
              <Image
                style={[
                  // { width: width/100*70, height: width/100*60 },
                  { width: width, height: height },
                  this.state.widthphoto > this.state.heightphoto && {
                    transform: [{ rotate: "90deg" }]
                  }
                ]}
                source={{
                  uri: this.state.previewimage
                }}
                resizeMode={"contain"}
              />
            </ImageZoom>
          )}
        </View>
      </View>
    )
  }

  onlibphoto(url, type, timestamp) {
    this.state.photos.push({
      image: {
        image: url,
        timestamp: timestamp,
        type: type
      }
    })
    this.setState({ display: "" })
  }

  onPhoto() {
    let now = moment().format("DD-MM-YYYY HH:mm:ss")
    const options = {
      pauseAfterCapture: false,
      quality: 1,
      skipProcessing: true
    }

    if (this.camera) {
      this.camera.takePictureAsync(options).then(data => {
        // this.setState({ display: "" })
        const uri = data.uri
        // BASE64
        const filepath = uri.split("//")[1]
        // console.log("uri: ", uri)
        RNFS.readFile(filepath, "base64").then(base => {
          const base64 = `data:image/jpeg;base64,${base}`
          // console.log("BASE64: ", base)
          this.saveImage(base, "camera", now)
        })
      })
    }
  }
  async saveImage(base64, type, timestamp) {
    processTitle = "etc"

    let extPath = null

    extPath = RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + "/BBI/Images/")
    let extPath2 = RNFS.mkdir(
      RNFS.ExternalStorageDirectoryPath + "/BBI/Images/resize"
    )
    // console.log(RNFS.ExternalStorageDirectoryPath)

    // console.log("extPath", extPath)
    let pic_id = Math.floor(Math.random() * (999 - 100 + 1) + 100)
    let name = pic_id + ".jpeg"
    let NEWPATH = null

    NEWPATH =
      (await RNFS.ExternalStorageDirectoryPath) +
      "/BBI/Images/" +
      processTitle +
      this.state.photos.length +
      "" +
      name

    // const path = RNFS.DocumentDirectoryPath + '/test.txt';
    // // const path = RNFS.DocumentDirectoryPath + '/test.txt';
    // console.log("path", path)
    // RNFS.writeFile(path, 'yoyo', 'utf8').then((success) => {
    //   // Alert.alert('DEBUG::done!');
    //   console.log("pathwriteFile", success)
    //  });

    await RNFS.writeFile(NEWPATH, base64, "base64").then(async () => {
      await CameraRoll.saveToCameraRoll("file://" + NEWPATH, "photo")
      // Image.getSize("file://" + NEWPATH, (width, height) => {
      //   console.log(`The image dimensions are ${width}x${height}`);
      //   const bundleid = DeviceInfo.getBundleId()
      // /data/user/0/com.pylonpile/Upload/
      // var new_path = ''
      // new_path = '/data/data/' + bundleid + '/Upload/'+ '/' + this.state.photos.length + '' + Math.floor(Math.random() * (999 - 100 + 1) + 100) + '.jpeg'
      // console.log('newpath', new_path)

      // ImageResizer.createResizedImage(NEWPATH, width, height, 'JPEG', 80,0,'/storage/emulated/0/BBI/Images/resize').then(async(response) => {
      //   if (type == "camera") {
      //     console.warn('resize image path', response.path)
      //     await this.state.photos.push({
      //       image: {
      //         pic_id: pic_id,
      //         image: "file://" + response.path,
      //         timestamp: timestamp,
      //         type: type
      //       }
      //     })
      //     this.setState({ newphto: "file://" + response.path }, () => {})
      //   } else {
      //     const time = this.formatDate(timestamp, "gallery")
      //     this.state.photos.push({
      //       image: "file://" + response.path,
      //       timestamp: time,
      //       type: type
      //     })
      //   }
      if (type == "camera") {
        // console.warn('resize image path', response.path)
        await this.state.photos.push({
          image: {
            pic_id: pic_id,
            image: "file://" + NEWPATH,
            timestamp: timestamp,
            type: type,
            path: NEWPATH
          }
        })
        this.setState({ newphto: "file://" + NEWPATH }, () => {})
      }
      //   // this.setState({ display: "" })
      // }).catch((err) => {
      //   console.error(`Couldn't resize: ${err.message}`);
      // })
      // }, (error) => {
      //   console.error(`Couldn't get the image size: ${error.message}`);
      // });
    })
    // console.log(NEWPATH)
  }

  async onDeleteItem(id) {
    // let testarr = [1,2,3]
    // let testarrnew = []
    // this.testarr = [1,2,3]
    // this.testarrnew = this.testarr;
    // this.testarrnew.splice(1, 1)
    // console.log(this.testarr,this.testarrnew)
    this.setState({ newphoto: this.state.photos }, () => {
      let array = [...this.state.newphoto]
      let index = 0
      let removedarr = []
      for (var i = 0; i < this.state.newphoto.length; i++) {
        // console.log("image photos[i]", this.state.newphoto[i].image.image)
        // console.log("this.state.newphoto[i].image.image",i,this.state.newphoto[i].image.image,id,this.state.newphoto[i].image.image == id)
        if (this.state.newphoto[i].image.image == id.image) {
          // console.log('image photos[i]', this.state.photos[i].image.image ==id)
          index = i

          // console.log("array.length bef",array.length,this.state.newphoto.length)
          let removed = array.splice(index, 1)
          // console.log("removed aft",removed)
          this.state.removedarr.push({
            id: this.state.newphoto[i].image.building_checklist_result_id
          })
          // console.log("this.state.removedarr", this.state.removedarr)
          this.setState({ photos: array }, () => {
            this.saveallpic()
          })
        }
      }
    })

    // Actions.refresh({test:"test"})
  }

  render() {
    if (this.state.display == "") {
      return this.renderlist()
    } else if (this.state.display == "camera") {
      return this.renderCamera()
    } else if (this.state.display == "photoLibrary") {
      return this.renderPhotoLibrary()
    } else if (this.state.display == "preview") {
      return this.renderPreview()
    }
  }
}

const mapStateToProps = state => {
  return {
    image: state.topic.image,
    token: state.auth.token,
    check: state.topic.check,
    checkerror: state.topic.checkerror,
    Mainwork: state.main.Allwork
  }
}

export default connect(
  mapStateToProps,
  actions
)(Cameraroll)
