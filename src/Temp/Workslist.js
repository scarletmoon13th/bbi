import React, { Component } from "react"
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  ActivityIndicator,
  ScrollView,
  NetInfo,
  AsyncStorage
} from "react-native"

import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { Icon } from "react-native-elements"
import { BLCK_COLOR, SUBCOLOR1 } from "../Constants/Color"
import styles from "./styles/Workslist.style"
import * as actions from "../Actions"

const height = Dimensions.get("window").height
const width = Dimensions.get("window").width
class Workslist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: false,
      loading: true,
      data: [
        { name: "โครงการ1", check: false },
        { name: "โครงการ2", check: false },
        { name: "โครงการ3", check: false }
      ],
      lockpress:false
    }
  }

  componentDidMount() {
    // this.props.testtext(this.props.token).then((test)=>{
    //   console.warn("test",test)

    // })
    NetInfo.isConnected.fetch().then(async isConnected => {
      // console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      if (isConnected == true) {
        try {
          const value = await AsyncStorage.getItem("PIC_DATA")
          if (value !== null) {
            // We have data!!
            
            let JSONvalue = JSON.parse(value)
            console.log("olddata Json", JSONvalue)
            let dataold = { olddata: JSONvalue, token: this.props.token }
            console.log(JSONvalue)
            this.props.fetchAllplusold(dataold)
          } else {
            console.log("null")
            if (this.props.token != null) {
              this.props.fetchAlldata(this.props.token)
            }
          }
        } catch (error) {
          // Error retrieving data
          console.log(error)
        }
      } else {
      }
    })
  }
  async componentWillReceiveProps(nextProps) {
    // if (nextProps.work != null) {
    //   if (nextProps.work.buildings.lenght != 0) {
    //     let add_check = nextProps.work.buildings
    //     for (i = 0; i < nextProps.work.buildings.length; i++) {
    //       add_check[i].check = false
    //     }
    //     if (i == nextProps.work.buildings.length) {
    //       this.setState({ data: add_check, loading: false })
    //     }
    //   }
    // }
    if (nextProps.Mainwork != null) {
      this.setState({ data: nextProps.Mainwork.buildings, loading: false })
    }
    if (nextProps.error != null) {
      try {
        const value = await AsyncStorage.getItem("MAIN_DATA")
        if (value !== null) {
          // We have data!!
          // console.log(value)
          let JSONvalue = JSON.parse(value)
          this.props.saveMaindata(JSONvalue)
        } else {
          console.log("null")
        }
      } catch (error) {
        // Error retrieving data
        console.log(error)
      }
      Alert.alert("Error", nextProps.error, [
        { text: "OK", onPress: () => console.log("OK Pressed out") }
      ])
    }
    this.setState({ loading: false })
  }
  oncheck(key) {
    let newArray = this.state.data
    newArray[key].check = !newArray[key].check
    this.setState({ data: newArray }, () => {})
  }

  onnamePress = item => {
    this.setState({lockpress:true},()=>{
      Actions.topiclist({ buildingsid: item.building_id, title: item.name })
    })
    
    setTimeout(()=>{this.setState({lockpress:false})},2000)
  }

  onlogout = async () => {
    Alert.alert("Logout", "หากคุณออกจากระบบข้อมูลทั้งหมดจะถูกลบ ต้องการดำเนินการต่อหรือไม่", [
      { text: "ตกลง", onPress: () => this.logout() },
      { text: "ยกเลิก", onPress: () => console.log("No Pressed out") }
    ])
    return
  }
  logout =async()=>{
    
    await AsyncStorage.multiRemove(["MAIN_DATA", "PIC_DATA", "USER_DATA"], err => {
      console.log("Local storage user info removed!")
    })
    Actions.pop()
  }

  render() {
    if (this.state.loading == true) {
      return (
        <View style={[styles.container, { justifyContent: "center" }]}>
          <ActivityIndicator size="large" color={SUBCOLOR1} />
        </View>
      )
    }
    return (
      <View style={[styles.container, {}]}>
        <View style={[styles.navigation, styles.navigationBlue]}>
          <View style={styles.navigationTitle}>
            <Text style={styles.navigationTitleText}>{"โครงการ"}</Text>
          </View>
          <TouchableOpacity
            style={styles.navigationLogout}
            onPress={this.onlogout}
          >
            <Icon
              size={(width / 360) * 30}
              name="ios-exit"
              type="ionicon"
              color={"white"}
            />
          </TouchableOpacity>
        </View>
        <ScrollView style={styles.boxRow}>
          {this.state.data != null &&
            this.state.data.map((item, index) => {
              return (
                <View style={styles.Itemsbox}>
                  {/*<TouchableOpacity
                style={styles.Innerstartsbox}
                onPress={() => {
                  this.oncheck(index)
                }}
              >
                {item.check == false ? (
                  <Icon name="square" type="font-awesome" color={BLCK_COLOR} />
                ) : (
                  <Icon
                    name="check-square"
                    type="font-awesome"
                    color={BLCK_COLOR}
                  />
                )}
                </TouchableOpacity>*/}
                  <TouchableOpacity
                    style={styles.Innermiddlebox}
                    onPress={() => this.onnamePress(item)}
                    disabled={this.state.lockpress}
                  >
                    <Text>{item.name}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.Innerendbox}
                    onPress={() => {
                      // console.warn(item.description)
                      Actions.WorkDetail({ data: item })
                    }}
                  >
                    <Icon
                      name="file-o"
                      type="font-awesome"
                      color={BLCK_COLOR}
                    />
                  </TouchableOpacity>
                </View>
              )
            })}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    work: state.work.work,
    Mainwork: state.main.Allwork,
    error: state.main.error
  }
}

export default connect(
  mapStateToProps,
  actions
)(Workslist)
