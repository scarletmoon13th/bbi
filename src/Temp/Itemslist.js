import React, { Component } from "react"
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  ActivityIndicator,
  FlatList
} from "react-native"

import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import { Icon } from "react-native-elements"
import styles from "./styles/Itemslist.style"
import * as actions from "../Actions"

const { height, width } = Dimensions.get("window")

class Itemslist extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      floorname: "",
      items: null
    }
  }

  componentDidMount() {
    // if (this.props.token != null) {
    //   this.props.fetchAlldata(this.props.token)
    // }

    if (this.props.title != null) {
      this.setState({ floorname: this.props.title })
    }

    if (
      this.props.buildingsid != null &&
      this.props.Mainwork.buildings != null &&
      this.props.floorid != null
    ) {
      this.props.Mainwork.buildings.map((item, index) => {
        // console.log(item.building_id ,this.props.buildingsid)
        if (item.building_id == this.props.buildingsid) {
          item.building_floors.map((item, index) => {
            // console.log(item)
            if (item.building_floor_id == this.props.floorid) {
              // console.log(this.props.building_floor_id)
              this.setState(
                { items: item.building_checklists, loading: false },
                () => {
                  // console.log(this.state.items)
                }
              )
            }
          })
          // this.setState({ topic: item.building_floors, loading: false })
        }
      })
    }
  }
  render() {
    // if (this.state.loading == true) {
    //   return (
    //     <View style={[styles.container, { justifyContent: "center" }]}>
    //       <ActivityIndicator size="large" color={SUBCOLOR1} />
    //     </View>
    //   )
    // }
    return (
      <View style={styles.container}>
        <View style={[styles.navigation, styles.navigationBlue]}>
          <View style={styles.navigationTitle}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.buttonRight}
              onPress={() => {
                // Actions.pop({
                //   test: "test"
                // })
                Actions.pop()
                setTimeout(() => {
                  Actions.refresh({
                    buildingsid: this.props.buildingsid, 
                    title: this.props.floors_name
                  })
                }, 0)
              }}
            >
              <Icon name="arrow-left" type="font-awesome" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.navigationTitleText}>
              {this.state.floorname}
            </Text>
          </View>
        </View>
        <FlatList
          data={this.state.items}
          renderItem={this.renderItem}
          numColumns={2}
          style={styles.bodybox}
          keyExtractor={index => {
            index.toString()
          }}
          keyboardDismissMode="on-drag"
          extraData={this.state}
          contentContainerStyle={{
            alignItems: "center",
            justifyContent: "center"
          }}
        />
      </View>
    )
  }

  renderItem = ({ item, index }) => {
    // console.log("item.check", item.name)
    return (
      <View style={[styles.titlebox]}>
        <TouchableOpacity
          style={styles.titleinnerbox}
          onPress={() => {
            // this.onItemPress(item)
            // Actions.itemslist()
            Actions.Cameraroll({
              title: item.name,
              buildingsid: this.props.buildingsid,
              floorid: item.building_floor_id,
              itemsid: item.building_checklist_id
            })
          }}
        >
          <View style={styles.Toptitlebox}>
            <Text style={styles.text}>{item.name}</Text>
          </View>
          <View style={styles.endtitlebox} />
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    work: state.work.work,
    Mainwork: state.main.Allwork,
    error: state.main.error
  }
}

export default connect(
  mapStateToProps,
  actions
)(Itemslist)
