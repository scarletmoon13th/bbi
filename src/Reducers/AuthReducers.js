import { LOGIN, ERROR_LOGIN } from "../Actions/types"

const INITIAL_STATE = {
  token: null,
  error: null,
  randomlogin:null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payload,
        error: null,
        randomlogin: null
      }
    case ERROR_LOGIN:
      return {
        ...state,
        token: null,
        error: action.payload,
        randomlogin: Math.random() * Math.random() *100
      }
    default:
      return state
  }
}
