import { WORK, WORKERROR } from "../Actions/types"

const INITIAL_STATE = {
  work: null,
  workerror: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case WORK:
      return {
        ...state,
        work: action.payload,
        workerror: null
      }
    case WORKERROR:
      return {
        ...state,
        work: null,
        workerror: action.payload
      }
    default:
      return state
  }
}
