import { MAIN_WORK, ERROR } from "../Actions/types"

const INITIAL_STATE = {
  Allwork: null,
  error: null,
  random:null
}

export default (state = INITIAL_STATE, action) => {
  // console.warn("action.payload")
  switch (action.type) {
    case ERROR:
    return {
      ...state,
      error: action.payload
    }
    case MAIN_WORK:

      return {
        ...state,
        Allwork: action.payload,
        error: null,
        random: Math.random() * Math.random() *100
      }
    
    default:
      return state
  }
}
