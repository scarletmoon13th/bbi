import { combineReducers } from "redux"
import AuthReducer from "./AuthReducers"
import WorkReducers from "./WorkReducers"
import TopicReducers from "./TopicReducers"
import MainReducers from "./MainReducers"
export default combineReducers({
  // Reducer
  auth: AuthReducer,
  work: WorkReducers,
  topic: TopicReducers,
  main: MainReducers
})
