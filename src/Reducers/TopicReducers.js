import {
  TOPIC,
  TOPICERROR,
  IMAGE,
  IMAGEERROR,
  CHECK,
  CHECK_ERROR,
  COUNT
} from "../Actions/types"

const INITIAL_STATE = {
  topic: null,
  topicerror: null,
  image: null,
  imageerror: null,
  check: null,
  checkerror: null,
  photocount: null,
  isLast:null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOPIC:
      return {
        ...state,
        topic: action.payload,
        topicerror: null
      }
    case TOPICERROR:
      return {
        ...state,
        topic: null,
        topicerror: action.payload
      }
    case IMAGE:
      return {
        ...state,
        image: action.payload,
        imageerror: null
      }
    case IMAGEERROR:
      return {
        ...state,
        image: null,
        imageerror: action.payload
      }
    case CHECK:
      return {
        ...state,
        check: action.payload,
        checkerror: null
      }
    case CHECK_ERROR:
      return {
        ...state,
        check: null,
        checkerror: action.payload,
        isLast:action.isLast
      }
    case COUNT:
      return {
        ...state,
        photocount: action.payload,
        checkerror: null,
        isLast:action.isLast
      }
    default:
      return state
  }
}
